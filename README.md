# DungeonExplorer




## Description
Dungeon Explorer is a 2D Platformer game written in Java and JavaFX. The goal is to make your way across the map to find the key that will allow you to open the gate to proceed to the next level. There are some traps and enemies in your way though, so be careful to not loose all your lives.

## Installation
The project is set up to run on Java 11 in a MacOS or Windows environment. To change the used Java version simply edit the corresponding line (sourceCompatibility) in the build.gradle file. To run the project on a Linux environment, simply add the missing dependencies in the build.gradle file (You can copy and paste the mac or win compiles and change mac or win to linux).

## Usage
The project can be executed by running the class Launcher located in the launcher package or by running the executable jar file located at the /build/libs folder.

Once you successfully started the project you can:

- In the main menu: 
    - Press the Play button to start playing level 1.
    - Press the Levels button to choose your desired level of the game (Only levels 1 - 3 are implemented so far).
    - Press the Settings button to change various settings (Not yet implemented).
    - Press the Help button to see the different controls (Not fully up-to-date as controls are continuously added at the moment).
    
- In the game: 
    - Use A and D keys to move and SPACE key to jump. You can jump against a vertical obstacle which will allow you to either slide down or wall jump your way up.
    - Use any Mouse Button to use you whip and slay some enemies.
    - Press I key to open the inventory and use collected potions.
    - Press ESC key to open the game menu.
    - Collect different collectibles. There is always 1 key per level which you'll have to find to open the gate to the next level. Once you have collected the key, move to the gate and press E key while you stand in front of it, then press W key to enter the gate.

- In the Game Over or Win screen:
    - Press the arrow in the top left corner to get back to the start window.

## Roadmap
This game is nowhere from finished and a lot of changes and additions are planned, like:
- Add more/different enemies (and maybe some boss enemies).
- Add more levels.
- Add different sound effects to the individual actions.
- Change life removal animation, does not work properly.
- Add as much animations as possible! :D
- Add settings and adjust help menu.
- Decide on and set effects to green and purple potions.
- And much more...

## Project status
This project is a work in progress and will change a lot over the next months. So stay tuned and make sure to test out future changes!
