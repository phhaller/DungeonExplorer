package enemy;

import attacks.AttackManager;
import collision.CollisionManager;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

public class Giant extends Enemy {

  private final int width = 64;
  private final int height = 64;

  private long currentElapsedTime = 0;
  private String movementDir = "LEFT";

  private long start = System.nanoTime();


  public Giant(double x, double y, double minX, double maxX, CollisionManager collisionManager, AttackManager attackManager) {

    super(x, y, minX, maxX, collisionManager, attackManager);

    createEnemy();
    createMovement();

  }


  @Override
  protected void createEnemy() {

    Image spriteImg = new Image("/spritesheet/giant.png");
    ImageView imageView = new ImageView(spriteImg);

    enemyAnimation = new EnemyAnimation(imageView, width, height, 4);
    enemyAnimation.setTranslateX(x);
    enemyAnimation.setTranslateY(y);
    enemyAnimation.setOpacity(.9);

    hitBox = new Rectangle(width, height);
    hitBox.translateXProperty().bind(enemyAnimation.translateXProperty());
    hitBox.translateYProperty().bind(enemyAnimation.translateYProperty());
    hitBox.getStyleClass().add("enemyRec");

  }

  @Override
  protected void createMovement() {

    movementTimeline = new Timeline(new KeyFrame(Duration.millis(10), new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {

        collisionManager.checkPlayerEnemyCollision();

        String dir = collisionManager.checkPlayerInSight(enemyAnimation.getTranslateX(), enemyAnimation.getTranslateY(), 400);

        if (!dir.isEmpty()) {

          if (!movementDir.equals(dir)) {
            movementDir = dir;
            start = System.nanoTime();
          } else {
            if (((System.nanoTime() - start) / 1000000000) % 5 == 0 && (System.nanoTime() - start) / 1000000000 != currentElapsedTime) {

              currentElapsedTime = (System.nanoTime() - start) / 1000000000;
              attackManager.createFireBeam(enemyAnimation.getTranslateX() + 32, enemyAnimation.getTranslateY() + 32, movementDir);


            }
          }

        } else {


        }

        enemyAnimation.animation.setOffsetY(0);
        enemyAnimation.animation.play();

      }
    }));

    movementTimeline.setCycleCount(Animation.INDEFINITE);
    movementTimeline.play();

  }

}
