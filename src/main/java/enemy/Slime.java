package enemy;

import attacks.AttackManager;
import collision.CollisionManager;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import java.util.Random;

public class Slime extends Enemy {

  private final int width = 32;
  private final int height = 32;

  private long currentElapsedTime = 0;
  private String movementDir = "";

  public Slime(double x, double y, double minX, double maxX, CollisionManager collisionManager, AttackManager attackManager) {
    super(x, y, minX, maxX, collisionManager, attackManager);
    createEnemy();
    createMovement();
  }

  @Override
  protected void createEnemy() {

    Image spriteImg = new Image("/spritesheet/slime.png");
    ImageView imageView = new ImageView(spriteImg);

    enemyAnimation = new EnemyAnimation(imageView, width, height, 7);
    enemyAnimation.setTranslateX(x);
    enemyAnimation.setTranslateY(y);
    enemyAnimation.setOpacity(.9);

    hitBox = new Rectangle(width, height);
    hitBox.translateXProperty().bind(enemyAnimation.translateXProperty());
    hitBox.translateYProperty().bind(enemyAnimation.translateYProperty());
    hitBox.getStyleClass().add("enemyRec");

  }

  @Override
  protected void createMovement() {

    long start = System.nanoTime();

    movementTimeline = new Timeline(new KeyFrame(Duration.millis(10), new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {

        collisionManager.checkPlayerEnemyCollision();

        String dir = collisionManager.checkPlayerInSight(enemyAnimation.getTranslateX(), enemyAnimation.getTranslateY(), 200);

        if (!dir.isEmpty()) {

          movementDir = dir;

        } else {

          if (((System.nanoTime() - start) / 1000000000) % 2 == 0 && (System.nanoTime() - start) / 1000000000 != currentElapsedTime) {

            currentElapsedTime = (System.nanoTime() - start) / 1000000000;
            Random rand = new Random();
            int r = rand.nextInt(3);

            if (r == 0) {
              movementDir = "LEFT";
            }

            if (r == 1) {
              movementDir = "RIGHT";
            }

            if (r == 2) {
              movementDir = "IDLE";
            }

          }

        }


        if (movementDir.equals("LEFT") && enemyAnimation.getTranslateX() > minX) {
          enemyAnimation.setTranslateX(enemyAnimation.getTranslateX() - .5);
          enemyAnimation.animation.setOffsetY(0);
          enemyAnimation.animation.play();
        }

        if (movementDir.equals("RIGHT") && enemyAnimation.getTranslateX() < maxX) {
          enemyAnimation.setTranslateX(enemyAnimation.getTranslateX() + .5);
          enemyAnimation.animation.setOffsetY(33);
          enemyAnimation.animation.play();
        }

        if (movementDir.equals("IDLE")) {
          // enemyAnimation.animation.setOffsetY(64);
        }


      }
    }));

    movementTimeline.setCycleCount(Animation.INDEFINITE);
    movementTimeline.play();

  }


}
