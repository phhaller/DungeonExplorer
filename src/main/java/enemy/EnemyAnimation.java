package enemy;

import javafx.geometry.Rectangle2D;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import utils.SpriteAnimation;


public class EnemyAnimation extends Pane {

  private ImageView imageView;
  private int count;
  private int columns;
  private int offsetX = 0;
  private int offsetY = 0;
  private int width;
  private int height;

  private Rectangle hitBox;

   public SpriteAnimation animation;


  public EnemyAnimation(ImageView imageView, int width, int height, int count) {
    this.imageView = imageView;
    this.width = width;
    this.height = height;
    this.imageView.setViewport(new Rectangle2D(offsetX, offsetY, width, height));
    this.count = count;
    this.columns = count;
    animation = new SpriteAnimation(imageView, Duration.millis(700), count, columns, offsetX, offsetY, width, height);
    hitBox = new Rectangle(offsetX, offsetY, width, height);
    hitBox.setStyle("-fx-stroke: transparent; -fx-fill: transparent");
    getChildren().addAll(imageView, hitBox);
  }


  public int getSpriteWidth() {
    return width;
  }

  public int getSpriteHeight() {
    return height;
  }

  public Rectangle getHitBox() {
    return hitBox;
  }

}