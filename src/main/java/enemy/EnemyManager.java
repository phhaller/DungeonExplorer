package enemy;

import attacks.AttackManager;
import collision.CollisionManager;
import javafx.scene.layout.AnchorPane;
import obstacle.Obstacle;

import java.util.ArrayList;
import java.util.Random;

public class EnemyManager {

  private int level;
  private AnchorPane contentPane;
  private ArrayList<Obstacle> obstacleList;
  private CollisionManager collisionManager;
  private AttackManager attackManager;

  private ArrayList<Enemy> enemiesList;


  public EnemyManager(int level, AnchorPane contentPane, ArrayList<Obstacle> obstacleList, CollisionManager collisionManager, AttackManager attackManager) {

    this.level = level;
    this.contentPane = contentPane;
    this.obstacleList = obstacleList;
    this.collisionManager = collisionManager;
    this.attackManager = attackManager;

    enemiesList = new ArrayList<>();
    creatEnemies();

  }


  private void creatEnemies() {

    int e = 0;

    switch (level) {
      case 1:
        e = 7;
        break;
      case 2:
        e = 10;
        break;
      case 3:
        e = 7;
        break;
      case 4:
        e = 23;
        break;
    }

    for (int i = 0; i < e; i++) {

      Random random = new Random();
      int rand = (int) random.nextInt(obstacleList.size());

      Obstacle o = obstacleList.get(rand);

      if (!o.getType().toLowerCase().contains("wall") && !o.getType().toLowerCase().contains("trap") &&
          o.getObstacle().getWidth() > 64 && !o.getHasEnemy()) {

          /*Enemy enemy = new Enemy(
              o.getObstacle().getTranslateX() + o.getObstacle().getWidth() / 2 - 16,
              o.getObstacle().getTranslateY() - 32,
              o.getObstacle().getTranslateX() + 5,
              o.getObstacle().getTranslateX() + o.getObstacle().getWidth() - 37,
              collisionManager);*/

        Enemy enemy = null;

        if (i < 5) {
          enemy = new Fire(
              o.getObstacle().getTranslateX() + o.getObstacle().getWidth() / 2 - 32,
              o.getObstacle().getTranslateY() - 64,
              o.getObstacle().getTranslateX() + 5,
              o.getObstacle().getTranslateX() + o.getObstacle().getWidth() - 37,
              collisionManager,
              attackManager);
        } else {
          enemy = new Slime(
              o.getObstacle().getTranslateX() + o.getObstacle().getWidth() / 2 - 16,
              o.getObstacle().getTranslateY() - 32,
              o.getObstacle().getTranslateX() + 5,
              o.getObstacle().getTranslateX() + o.getObstacle().getWidth() - 37,
              collisionManager,
              attackManager);
        }

        enemiesList.add(enemy);

        o.setHasEnemy(true);
        contentPane.getChildren().add(enemy.getEnemy());

      } else {
        e++;
      }

    }

  }





  public ArrayList<Enemy> getEnemiesList() {
    return enemiesList;
  }


}
