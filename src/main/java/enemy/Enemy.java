package enemy;

import attacks.AttackManager;
import collision.CollisionManager;
import effect.EffectManager;
import javafx.animation.Timeline;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import utils.SpriteAnimation;

public abstract class Enemy {

  protected double x;
  protected double y;
  protected double minX;
  protected double maxX;
  protected CollisionManager collisionManager;
  protected AttackManager attackManager;
  protected EffectManager effectManager;

  protected Rectangle hitBox;
  protected EnemyAnimation enemyAnimation;
  protected Timeline movementTimeline;

  protected boolean alive = true;


  public Enemy(double x, double y, double minX, double maxX, CollisionManager collisionManager, AttackManager attackManager) {

    this.x = x;
    this.y = y;
    this.minX = minX;
    this.maxX = maxX;
    this.collisionManager = collisionManager;
    this.attackManager = attackManager;

  }


  protected abstract void createEnemy();

  protected abstract void createMovement();

  public void kill(AnchorPane contentPane) {
    alive = false;
    movementTimeline.stop();
    enemyAnimation.animation.stop();
    effectManager.play("enemyHit", enemyAnimation.getTranslateX(), enemyAnimation.getTranslateY());
    contentPane.getChildren().remove(enemyAnimation);
  }

  public void setEffectManager(EffectManager effectManager) {
    this.effectManager = effectManager;
  }

  public Pane getEnemy() {
    return enemyAnimation;
  }

  public Timeline getMovementTimeline() {
    return movementTimeline;
  }

  public SpriteAnimation getAnimation() {
    return enemyAnimation.animation;
  }

  public boolean isAlive() {
    return alive;
  }

}
