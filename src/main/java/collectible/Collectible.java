package collectible;

import background.Chunk;
import effect.EffectManager;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.RotateTransition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

public class Collectible {

  private AnchorPane background;
  private Rectangle collectible;
  private String type;
  private int width = 32;
  private int height = 32;
  private int x;
  private int y;
  private boolean collected = false;

  private EffectManager effectManager;


  public Collectible(int x, int y, String type, AnchorPane background) {

    this.x = x;
    this.y = y;
    this.type = type;
    this.background = background;

    createCollectible();

  }


  private void createCollectible() {

    collectible = new Rectangle(width, height);
    collectible.setTranslateX(x);
    collectible.setTranslateY(y);

    if (type.equals("coin")) {
      collectible.getStyleClass().add("coin");
    }

    if (type.equals("key")) {
      collectible.getStyleClass().add("key");
    }

    if (type.contains("Potion")) {
      if (type.contains("Green")) {
        collectible.getStyleClass().add("potionGreen");
      }

      if (type.contains("Blue")) {
        collectible.getStyleClass().add("potionBlue");
      }

      if (type.contains("Purple")) {
        collectible.getStyleClass().add("potionPurple");
      }

      if (type.contains("Red")) {
        collectible.getStyleClass().add("potionRed");
      }

      if (type.contains("Yellow")) {
        collectible.getStyleClass().add("potionYellow");
      }
    }

  }


  public void collect(Chunk cHunk) {

    collected = true;
    System.out.println(type + " COLLECCTED");

    RotateTransition rt = new RotateTransition(Duration.millis(1000), collectible);
    rt.setAxis(Rotate.Y_AXIS);
    rt.setByAngle(180);
    rt.setCycleCount(1);

    FadeTransition ft = new FadeTransition(Duration.millis(1000), collectible);
    ft.setFromValue(1.0);
    ft.setToValue(0.0);
    ft.setCycleCount(1);

    TranslateTransition tt = new TranslateTransition(Duration.millis(1000), collectible);
    tt.setFromY(y);
    tt.setToY(y - 20);
    tt.setCycleCount(1);

    ParallelTransition pt = new ParallelTransition(rt, ft, tt);
    pt.play();

    rt.setOnFinished(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        cHunk.removeCollectibleFromChunk(Collectible.this);
        background.getChildren().remove(collectible);
      }
    });

  }



  public Rectangle getCollectible() {
    return collectible;
  }

  public String getType() {
    return type;
  }

  public boolean isCollected() {
    return collected;
  }

  public void setEffectManager(EffectManager effectManager) {
    this.effectManager = effectManager;
  }



}
