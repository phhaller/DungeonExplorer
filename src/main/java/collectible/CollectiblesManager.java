package collectible;


import javafx.scene.layout.AnchorPane;
import utils.CSVReader;

import java.util.ArrayList;

public class CollectiblesManager {

  private ArrayList<Collectible> collectibles;
  private AnchorPane contentPane;
  private int level;
  private String[][] collectibleTypes;


  public CollectiblesManager(AnchorPane contentPane, int level) {

    this.contentPane = contentPane;
    this.level = level;
    collectibles = new ArrayList<>();
    readCollectibles();
    creatCollectibles();

  }


  private void readCollectibles() {

    CSVReader reader = new CSVReader("Collectibles" + level + ".csv");
    collectibleTypes = reader.getCsvMap();

  }


  private void creatCollectibles() {

    for (int x = 0; x < collectibleTypes.length; x++) {
      for (int y = 0; y < collectibleTypes[0].length; y++) {

        String type = collectibleTypes[x][y];

        if (!type.contains("NULL")) {

          if (type.contains("collectible")) {

            if (type.contains("Coin")) {
              Collectible collectible = new Collectible(x * 64 + 16, y * 64 + 16, "coin", contentPane);
              collectibles.add(collectible);
            }

            if (type.contains("Key")) {
              Collectible collectible = new Collectible(x * 64 + 16, y * 64 + 16, "key", contentPane);
              collectibles.add(collectible);
            }

            if (type.contains("Potion")) {
              Collectible collectible = new Collectible(x * 64 + 16, y * 64 + 16, type.substring(11), contentPane);
              collectibles.add(collectible);
            }

          }

        }

      }

    }

  }



  public ArrayList<Collectible> getCollectibles() {
    return collectibles;
  }

}
