package menu;

import database.SettingsDatabaseManager;
import effect.SoundManager;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.util.ArrayList;

public class SettingsMenu {

  private SettingsDatabaseManager settingsDatabaseManager;

  private String status;
  private AnchorPane settingsMenuPane;
  private Label backLabel;
  private ArrayList<Slider> soundSliders;


  public SettingsMenu(String status) {

    this.status = status;
    createSettingsMenu();
    createHandlers();

  }


  private void createSettingsMenu() {

    settingsMenuPane = new AnchorPane();
    settingsMenuPane.setPrefSize(1280, 768);
    settingsMenuPane.setTranslateX(0);
    settingsMenuPane.setTranslateY(0);
    settingsMenuPane.getStyleClass().add("settingsMenu");

    backLabel = new Label();
    backLabel.setTranslateX(30);
    backLabel.setTranslateY(30);
    backLabel.setGraphic(new ImageView(new Image("/pictures/menu/backArrow.png")));
    backLabel.getStyleClass().add("backLabel");


    Label settingsTitleLabelSound = new Label("Sounds");
    settingsTitleLabelSound.setPrefSize(200, 35);
    settingsTitleLabelSound.setTranslateX(300);
    settingsTitleLabelSound.setTranslateY(100);
    settingsTitleLabelSound.getStyleClass().add("settingsTitleLabel");

    soundSliders = new ArrayList<>();

    Group stompGroup = createSoundSliderGroup("Stomp", 60, 300, 150);
    Group whipGroup = createSoundSliderGroup("Whip", 50, 300, 190);
    Group slimeGroup = createSoundSliderGroup("Slime", 100, 300, 230);
    Group gateGroup = createSoundSliderGroup("Gate", 100, 300, 270);
    Group musicGroup = createSoundSliderGroup("Music", 100, 300, 310);


    settingsMenuPane.getChildren().addAll(backLabel, settingsTitleLabelSound, stompGroup, whipGroup, slimeGroup, gateGroup, musicGroup);

    settingsMenuPane.setVisible(false);

  }


  private void createHandlers() {

    backLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        if (settingsDatabaseManager != null) {
          if (settingsDatabaseManager.getIdEntry(1.0).isEmpty()) {
            settingsDatabaseManager.createEntry(1.0, .6, .5, 1.0, 1.0, 1.0);
          } else {
            settingsDatabaseManager.updateEntry(1.0,
                (1.0 - 0.0) * (soundSliders.get(0).getValue() - 0.0) / (100.0 - 0.0) + 0.0,
                (1.0 - 0.0) * (soundSliders.get(1).getValue() - 0.0) / (100.0 - 0.0) + 0.0,
                (1.0 - 0.0) * (soundSliders.get(2).getValue() - 0.0) / (100.0 - 0.0) + 0.0,
                (1.0 - 0.0) * (soundSliders.get(3).getValue() - 0.0) / (100.0 - 0.0) + 0.0,
                (1.0 - 0.0) * (soundSliders.get(4).getValue() - 0.0) / (100.0 - 0.0) + 0.0);
          }
        }

        settingsDatabaseManager.closeConnection();
        settingsMenuPane.setVisible(false);

        if (status.equals("game")) {
          SoundManager.updateVolumes();
        }

      }
    });

  }


  private Group createSoundSliderGroup(String soundType, int value, int x, int y) {

    Group soundSliderGroup = new Group();
    soundSliderGroup.setTranslateX(x);
    soundSliderGroup.setTranslateY(y);

    Label soundTypeLabel = new Label(soundType);
    soundTypeLabel.setPrefSize(200, 40);
    soundTypeLabel.getStyleClass().add("soundTypeLabel");

    Slider soundSlider = new Slider();
    soundSlider.setPrefSize(300, 40);
    soundSlider.setTranslateX(200);
    soundSlider.setMin(0);
    soundSlider.setMax(100);
    soundSlider.setValue(value);
    soundSlider.setBlockIncrement(1);
    soundSlider.setMajorTickUnit(1);
    soundSlider.setMinorTickCount(0);
    soundSlider.setSnapToTicks(true);

    soundSliders.add(soundSlider);

    Label sliderValueLabel = new Label(String.valueOf(value));
    sliderValueLabel.textProperty().bind(soundSlider.valueProperty().asString());
    sliderValueLabel.setPrefSize(100, 40);
    sliderValueLabel.setTranslateX(500);
    sliderValueLabel.getStyleClass().addAll("soundTypeLabel", "center");

    soundSlider.valueProperty().addListener((obs, oldval, newVal) ->
        soundSlider.setValue(newVal.intValue()));

    soundSliderGroup.getChildren().addAll(soundTypeLabel, soundSlider, sliderValueLabel);

    return soundSliderGroup;

  }


  public void setVisible() {
    settingsDatabaseManager = new SettingsDatabaseManager();
    updateSLidersAndLabels();
    settingsMenuPane.setVisible(true);
  }


  private void updateSLidersAndLabels() {

    ArrayList<String> soundEntries = settingsDatabaseManager.getIdEntry(1.0);
    String[] entries = soundEntries.get(0).split("§");
    for (int i = 0; i < soundSliders.size(); i++) {
      soundSliders.get(i).setValue((100.0 - 0.0) * (Double.parseDouble(entries[i+1]) - 0.0) / (1.0 - 0.0) + 0.0);
    }



  }





  public AnchorPane getSettingsMenu() {
    return settingsMenuPane;
  }




}
