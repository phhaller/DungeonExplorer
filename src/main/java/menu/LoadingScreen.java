package menu;

import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

public class LoadingScreen {

  private int level;
  private AnchorPane loadingScreen;
  private Label levelLabel;


  public LoadingScreen(int level) {

    this.level = level;
    createLoadingScreen();

  }


  private void createLoadingScreen() {

    loadingScreen = new AnchorPane();
    loadingScreen.setPrefSize(1280, 768);
    loadingScreen.getStyleClass().add("loadingScreen");

    levelLabel = new Label("LEVEL " + level);
    levelLabel.setPrefSize(680, 100);
    levelLabel.setTranslateX(300);
    levelLabel.setTranslateY(334);
    levelLabel.getStyleClass().add("loadingScreenLevel");

    loadingScreen.getChildren().add(levelLabel);
    loadingScreen.setVisible(false);

  }





  public AnchorPane getLoadingScreen() {
    return loadingScreen;
  }

  public Label getLevelLabel() {
    return levelLabel;
  }


}
