package menu;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

public class HelpMenu {

  private AnchorPane helpMenuPane;
  private Label backLabel;


  public HelpMenu() {

    createHelpMenu();
    createHandlers();

  }


  private void createHelpMenu() {

    try {
      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/helpMenuWindow.fxml"));
      helpMenuPane = loader.load();
    } catch (IOException e) {
      e.printStackTrace();
    }

    backLabel = new Label();
    backLabel.setTranslateX(30);
    backLabel.setTranslateY(30);
    backLabel.setGraphic(new ImageView(new Image("/pictures/menu/backArrow.png")));
    backLabel.getStyleClass().add("backLabel");

    helpMenuPane.getChildren().add(backLabel);

    helpMenuPane.setVisible(false);

  }


  private void createHandlers() {

    backLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        helpMenuPane.setVisible(false);
      }
    });

  }






  public AnchorPane getHelpMenu() {
    return helpMenuPane;
  }

}
