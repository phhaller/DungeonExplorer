package menu;

import inventory.Inventory;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import utils.AnimationEventManager;
import view.StartWindowController;

import java.io.IOException;

public class GameMenu {

  private Scene scene;
  private ScrollPane scrollPane;
  private AnchorPane gameMenuPane;

  private EventHandler<KeyEvent> eventHandler;

  private Label closeLabel;
  private Label settingsLabel;
  private SettingsMenu settingsMenu;
  private Label helpLabel;
  private HelpMenu helpMenu;
  private Label quitGameLabel;

  private Inventory inventory;

  private AnimationEventManager animationEventManager;


  public GameMenu(Scene scene) {
    this.scene = scene;
    settingsMenu = new SettingsMenu("game");
    helpMenu = new HelpMenu();
    createGameMenu();
    createHandlers();
  }


  private void createGameMenu() {

    gameMenuPane = new AnchorPane();
    gameMenuPane.setPrefSize(1280, 768);
    gameMenuPane.setTranslateX(0);
    gameMenuPane.setTranslateY(1152);
    gameMenuPane.getStyleClass().add("gameMenu");

    closeLabel = new Label();
    closeLabel.setTranslateX(30);
    closeLabel.setTranslateY(30);
    closeLabel.setGraphic(new ImageView(new Image("/pictures/menu/closeIcon.png")));
    closeLabel.getStyleClass().add("closeLabel");

    settingsLabel = new Label("Settings");
    settingsLabel.setPrefSize(200, 50);
    settingsLabel.setTranslateX(540);
    settingsLabel.setTranslateY(299);
    settingsLabel.getStyleClass().add("menuLabel");

    helpLabel = new Label("Help");
    helpLabel.setPrefSize(200, 50);
    helpLabel.setTranslateX(540);
    helpLabel.setTranslateY(359);
    helpLabel.getStyleClass().add("menuLabel");

    quitGameLabel = new Label("Quit");
    quitGameLabel.setPrefSize(200, 50);
    quitGameLabel.setTranslateX(540);
    quitGameLabel.setTranslateY(419);
    quitGameLabel.getStyleClass().add("menuLabel");

    AnchorPane settingsMenuPane = settingsMenu.getSettingsMenu();
    AnchorPane helpMenuPane = helpMenu.getHelpMenu();

    gameMenuPane.getChildren().addAll(closeLabel, settingsLabel, helpLabel, quitGameLabel, settingsMenuPane, helpMenuPane);

    gameMenuPane.setVisible(false);


  }



  private void createHandlers() {

    eventHandler = new EventHandler<KeyEvent>() {
      @Override
      public void handle(KeyEvent event) {

        if (event.getCode() == KeyCode.ESCAPE) {

          if (!inventory.getInventory().isVisible()) {

            System.out.println("OPEN THE GAME MENU");

            if (gameMenuPane.isVisible() && !settingsMenu.getSettingsMenu().isVisible()) {
              gameMenuPane.setVisible(false);
              settingsMenu.getSettingsMenu().setVisible(false);
              helpMenu.getHelpMenu().setVisible(false);
            } else {
              gameMenuPane.setTranslateX(scrollPane.getViewportBounds().getMinX() * -1);
              gameMenuPane.setTranslateY(scrollPane.getViewportBounds().getMinY() * -1);
              gameMenuPane.setVisible(true);
            }

          } else {
            inventory.getInventory().setVisible(false);
          }

        }

      }
    };

    scene.addEventHandler(KeyEvent.KEY_PRESSED, eventHandler);


    closeLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        gameMenuPane.setVisible(false);
      }
    });


    settingsLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        System.out.println("Settings clicked");
        settingsMenu.setVisible();
    }
    });


    helpLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        System.out.println("Help clicked");
        helpMenu.getHelpMenu().setVisible(true);
      }
    });


    quitGameLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        animationEventManager.stopGame();

        switchScene();
      }
    });

  }


  private void switchScene() {

    try {
      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/startWindow.fxml"));
      Parent root = loader.load();
      Stage stage = (Stage) gameMenuPane.getScene().getWindow();

      StartWindowController controller = loader.getController();
      controller.setScene(scene);

      stage.getScene().setRoot(root);

    } catch (IOException e) {
      e.printStackTrace();
    }

  }




  public void setScrollPane(ScrollPane scrollPane) {
    this.scrollPane = scrollPane;
  }

  public AnchorPane getGameMenu() {
    return gameMenuPane;
  }

  public void setInventory(Inventory inventory) {
    this.inventory = inventory;
  }

  public EventHandler<KeyEvent> getEventHandler() {
    return eventHandler;
  }

  public void setAnimationEventManager(AnimationEventManager animationEventManager) {
    this.animationEventManager = animationEventManager;
  }
}
