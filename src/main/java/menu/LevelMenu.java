package menu;

import database.HighscoreDatabaseManager;
import javafx.animation.AnimationTimer;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;
import view.GameWindowController;

import java.io.IOException;
import java.util.ArrayList;

public class LevelMenu {

  private Scene scene;
  private HighscoreDatabaseManager highscoreDatabaseManager;
  private AnchorPane contentPane;
  private ScrollPane scrollPane;
  private AnchorPane levelMenuPane;
  private Label backLabel;
  private ArrayList<Group> levelGroups;
  private Label nextPageLabel;
  private Label previousPageLabel;
  private int levelCounter = 0;
  private int page = 0;


  public LevelMenu(Scene scene, HighscoreDatabaseManager highscoreDatabaseManager) {

    this.scene = scene;
    this.highscoreDatabaseManager = highscoreDatabaseManager;
    createPanes();
    createLevelMenu();
    createHandlers();

  }


  private void createPanes() {

    levelMenuPane = new AnchorPane();
    levelMenuPane.setPrefSize(1280, 768);
    levelMenuPane.setTranslateX(0);
    levelMenuPane.setTranslateY(0);
    levelMenuPane.getStyleClass().add("levelMenu");

    contentPane = new AnchorPane();
    contentPane.setPrefSize(3970, 768);
    contentPane.getStyleClass().add("levelContentPane");

    scrollPane = new ScrollPane();
    scrollPane.setContent(contentPane);
    scrollPane.setPrefViewportWidth(1000);
    scrollPane.setPrefViewportHeight(768);
    scrollPane.setTranslateX(140);
    scrollPane.setVvalue(0);
    scrollPane.hbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.NEVER);
    scrollPane.vbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.NEVER);
    scrollPane.getStyleClass().add("levelScrollPane");

    levelMenuPane.getChildren().addAll(scrollPane);
    levelMenuPane.setVisible(false);

  }


  private void createLevelMenu() {

    backLabel = new Label();
    backLabel.setTranslateX(30);
    backLabel.setTranslateY(30);
    backLabel.setGraphic(new ImageView(new Image("/pictures/menu/backArrow.png")));
    backLabel.getStyleClass().add("backLabel");

    previousPageLabel = new Label();
    previousPageLabel.setTranslateX(598);
    previousPageLabel.setTranslateY(690);
    previousPageLabel.setGraphic(new ImageView(new Image("/pictures/menu/previousArrow.png")));
    previousPageLabel.getStyleClass().add("previousLabel");
    previousPageLabel.setDisable(true);

    nextPageLabel = new Label();
    nextPageLabel.setTranslateX(650);
    nextPageLabel.setTranslateY(690);
    nextPageLabel.setGraphic(new ImageView(new Image("/pictures/menu/nextArrow.png")));
    nextPageLabel.getStyleClass().add("nextLabel");

    Group level1 = createLevelGroup(1);
    Group level2 = createLevelGroup(2);
    Group level3 = createLevelGroup(3);
    Group level4 = createLevelGroup(4);
    Group level5 = createLevelGroup(5);
    Group level6 = createLevelGroup(6);
    Group level7 = createLevelGroup(7);
    Group level8 = createLevelGroup(8);
    Group level9 = createLevelGroup(9);
    Group level10 = createLevelGroup(10);
    Group level11 = createLevelGroup(11);
    Group level12 = createLevelGroup(12);

    levelGroups = new ArrayList<>();

    levelGroups.add(level1);
    levelGroups.add(level2);
    levelGroups.add(level3);
    levelGroups.add(level4);
    levelGroups.add(level5);
    levelGroups.add(level6);
    levelGroups.add(level7);
    levelGroups.add(level8);
    levelGroups.add(level9);
    levelGroups.add(level10);
    levelGroups.add(level11);
    levelGroups.add(level12);

    levelMenuPane.getChildren().addAll(backLabel, previousPageLabel, nextPageLabel);
    contentPane.getChildren().addAll(level1, level2, level3, level4, level5, level6, level7, level8, level9,
        level10, level11, level12);

  }


  private Group createLevelGroup(int level) {

    Group levelGroup = new Group();
    levelGroup.setTranslateX(20 + levelCounter * 300 + levelCounter * 30);
    levelGroup.setTranslateY(134);

    Rectangle groupBG = new Rectangle(300, 500);
    groupBG.setArcWidth(15);
    groupBG.setArcHeight(15);
    groupBG.getStyleClass().add("levelGroupBG");

    Rectangle levelImg = new Rectangle(200, 250);
    levelImg.setTranslateX(50);
    levelImg.setTranslateY(50);
    levelImg.setArcWidth(7);
    levelImg.setArcHeight(7);
    levelImg.getStyleClass().add("level" + level + "Img");

    Label levelLabel = new Label("LEVEL " + level);
    levelLabel.setPrefSize(250, 50);
    levelLabel.setTranslateX(25);
    levelLabel.setTranslateY(310);
    levelLabel.getStyleClass().add("levelLabel");

    String levelDesc = "";
    if (level == 1) {
      levelDesc = "Learn the basics and get ready for the real thing!";
    } else if (level == 2) {
      levelDesc = "Your first challenge! \n Can you find the key?";
    } else if (level == 3) {
      levelDesc = "Collectibles can be the key!";
    } else if (level == 4) {
      levelDesc = "Collectibles can be the key!";
    } else if (level == 5) {
      levelDesc = "Collectibles can be the key!";
    } else if (level == 6) {
      levelDesc = "Collectibles can be the key!";
    } else if (level == 7) {
      levelDesc = "Collectibles can be the key!";
    } else if (level == 8) {
      levelDesc = "Collectibles can be the key!";
    } else if (level == 9) {
      levelDesc = "Collectibles can be the key!";
    } else if (level == 10) {
      levelDesc = "Collectibles can be the key!";
    } else if (level == 11) {
      levelDesc = "Collectibles can be the key!";
    } else if (level == 12) {
      levelDesc = "Collectibles can be the key!";
    }

    Label levelDescLabel = new Label(levelDesc);
    levelDescLabel.setPrefSize(250, 50);
    levelDescLabel.setTranslateX(25);
    levelDescLabel.setTranslateY(360);
    levelDescLabel.getStyleClass().add("levelDescLabel");

    Label levelHighscoreLabel = new Label("HIGHSCORE: " + highscoreDatabaseManager.getLevelHighscore(level));
    levelHighscoreLabel.setPrefSize(250, 50);
    levelHighscoreLabel.setTranslateX(25);
    levelHighscoreLabel.setTranslateY(410);
    levelHighscoreLabel.getStyleClass().add("levelHighscoreLabel");

    String levelDifficulty = "";
    if (level == 1) {
      levelDifficulty = "INTRODUCTION";
    } else if (level == 2) {
      levelDifficulty = "BEGINNER";
    } else if (level == 3) {
      levelDifficulty = "BEGINNER";
    } else if (level == 4) {
      levelDifficulty = "NOVICE";
    } else if (level == 5) {
      levelDifficulty = "NOVICE";
    } else if (level == 6) {
      levelDifficulty = "NOVICE";
    } else if (level == 7) {
      levelDifficulty = "MASTER";
    } else if (level == 8) {
      levelDifficulty = "MASTER";
    } else if (level == 9) {
      levelDifficulty = "MASTER";
    } else if (level == 10) {
      levelDifficulty = "ELITE";
    } else if (level == 11) {
      levelDifficulty = "ELITE";
    } else if (level == 12) {
      levelDifficulty = "ELITE";
    }

    Label levelDifficultyLabel = new Label(levelDifficulty);
    levelDifficultyLabel.setPrefSize(250, 50);
    levelDifficultyLabel.setTranslateX(25);
    levelDifficultyLabel.setTranslateY(445);
    levelDifficultyLabel.getStyleClass().add("levelDifficultyLabel");

    groupBG.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        System.out.println(level);
        if (level <= 4) {
          switchScene(level);
        }
      }
    });

    levelGroup.getChildren().addAll(levelImg, levelLabel, levelDescLabel, levelHighscoreLabel, levelDifficultyLabel, groupBG);

    levelCounter++;

    return levelGroup;

  }


  private void createHandlers() {

    backLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        levelMenuPane.setVisible(false);
      }
    });

    previousPageLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        if (page > 0) {

          nextPageLabel.setDisable(false);
          if (page == 1) {
            previousPageLabel.setDisable(true);
          }

          page--;

          new AnimationTimer() {
            @Override
            public void handle(long now) {

              if (scrollPane.getHvalue() > page * (1.0/3.0) + 0.015) {
                scrollPane.setHvalue(scrollPane.getHvalue() - 0.015);
              } else {
                scrollPane.setHvalue(page * (1.0/3.0));
                stop();
              }

            }
          }.start();

        }

      }
    });

    nextPageLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        if (page < 3) {

          previousPageLabel.setDisable(false);
          if (page == 2) {
            nextPageLabel.setDisable(true);
          }

          page++;

          new AnimationTimer() {
            @Override
            public void handle(long now) {

              if (scrollPane.getHvalue() < page * (1.0/3.0) - 0.015) {
                scrollPane.setHvalue(scrollPane.getHvalue() + 0.015);
              } else {
                scrollPane.setHvalue(page * (1.0/3.0));
                stop();
              }

            }
          }.start();

        }

      }
    });

  }


  private void switchScene(int level) {

    highscoreDatabaseManager.closeConnection();

    LoadingScreen loadingScreen = new LoadingScreen(level);
    levelMenuPane.getChildren().add(loadingScreen.getLoadingScreen());

    try {
      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/gameWindow.fxml"));
      Parent root = loader.load();
      Stage stage = (Stage) levelMenuPane.getScene().getWindow();

      GameWindowController controller = loader.getController();
      controller.setScene(scene);
      controller.setLevel(level);

      loadingScreen.getLoadingScreen().setVisible(true);

      FadeTransition ft = new FadeTransition(Duration.millis(2000), loadingScreen.getLevelLabel());
      ft.setFromValue(0.0);
      ft.setToValue(1.0);
      ft.setCycleCount(1);
      ft.setAutoReverse(true);

      ft.play();

      ft.setOnFinished(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
          stage.getScene().setRoot(root);

        }
      });


    } catch (IOException e) {
      e.printStackTrace();
    }


  }



  public void setMaxLevel(int level) {

    for (int i = 0; i < levelGroups.size(); i++) {

      if (i < level) {
        levelGroups.get(i).setDisable(false);
      } else {
        levelGroups.get(i).setDisable(true);
      }

    }

  }

  public AnchorPane getLevelMenuPane() {
    return levelMenuPane;
  }




}
