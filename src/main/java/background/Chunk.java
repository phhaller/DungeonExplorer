package background;

import collectible.Collectible;
import javafx.scene.shape.Rectangle;
import obstacle.Obstacle;

import java.util.ArrayList;

public class Chunk {

  private int x;
  private int y;
  private double size;
  private Rectangle chunk;
  private ArrayList<Obstacle> obstaclesInChunk = new ArrayList<>();
  private ArrayList<Collectible> collectiblesInChunk = new ArrayList<>();


  public Chunk(int x, int y, double size) {

    this.x = x;
    this.y = y;
    this.size = size;

    createChunk();

  }


  private void createChunk() {

    chunk = new Rectangle(size, size);
    chunk.setTranslateX(x);
    chunk.setTranslateY(y);
    chunk.getStyleClass().add("chunk");

  }






  public void addObstacleToChunk(Obstacle obstacle) {
    obstaclesInChunk.add(obstacle);
  }

  public ArrayList<Obstacle> getObstaclesInChunk() {
    return obstaclesInChunk;
  }

  public void addCollectibleToChunk(Collectible collectible) {
    collectiblesInChunk.add(collectible);
  }

  public void removeCollectibleFromChunk(Collectible collectible) {
    collectiblesInChunk.remove(collectible);
  }

  public ArrayList<Collectible> getCollectiblesInChunk() {
    return collectiblesInChunk;
  }

  public Rectangle getChunk() {
    return chunk;
  }

}
