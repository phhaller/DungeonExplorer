package background;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import player.Player;
import player.PlayerAnimation;

public class Background {

  private int level;

  private int contentWidth;
  private int contentHeight;
  private int scrollWidth;
  private int scrollHeight;

  private AnchorPane contentPane;
  private ScrollPane scrollPane;

  private Player player;
  private PlayerAnimation playerAnimation;
  private Timeline scrollTimeline;



  public Background(int level, int contentWidth, int contentHeight, int scrollWidth, int scrollHeight) {

    this.level = level;
    this.contentWidth = contentWidth;
    this.contentHeight = contentHeight;
    this.scrollWidth = scrollWidth;
    this.scrollHeight = scrollHeight;

    createBackground();

  }


  private void createBackground() {

    contentPane = new AnchorPane();
    contentPane.setPrefSize(contentWidth, contentHeight);
    contentPane.getStyleClass().add("contentPaneBackground" + level);

    scrollPane = new ScrollPane();
    scrollPane.setContent(contentPane);
    scrollPane.setPrefViewportWidth(scrollWidth);
    scrollPane.setPrefViewportHeight(scrollHeight);
    scrollPane.setVvalue(1);
    scrollPane.hbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.NEVER);
    scrollPane.vbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.NEVER);
    scrollPane.addEventFilter(ScrollEvent.SCROLL, new EventHandler<ScrollEvent>() {
      @Override
      public void handle(ScrollEvent event) {
        event.consume();
      }
    });

    scrollPane.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        event.consume();
      }
    });

  }




  public void setPlayer(Player player) {
    this.player = player;
    playerAnimation = player.getPlayerAnimation();
    createScrollDetection();
  }


  private void createScrollDetection() {

    float movementXDivider = 9600f - 1280f;
    float speed = 1;

    scrollTimeline = new Timeline(new KeyFrame(Duration.millis(1), new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {

        if (playerAnimation.getTranslateX() > scrollPane.getHvalue() * (9600f - 1280f) + 1280f - 300f && scrollPane.getHvalue() < 1) {
          scrollPane.setHvalue(scrollPane.getHvalue() + speed / movementXDivider);
          // player.getHealth().getHealthGroup().setTranslateX(scrollPane.getViewportBounds().getMinX() * -1 + 1280 - player.getHealth().getHealthGroupWidth() - 32);
          player.getHealth().getHealthGroup().setTranslateX(player.getHealth().getHealthGroup().getTranslateX() + 1);
        }

        if (playerAnimation.getTranslateX() < scrollPane.getHvalue() * (9600f - 1280f) + 300f && scrollPane.getHvalue() > 0) {
          scrollPane.setHvalue(scrollPane.getHvalue() - speed / movementXDivider);
          // player.getHealth().getHealthGroup().setTranslateX(scrollPane.getViewportBounds().getMinX() * -1 + 1280 - player.getHealth().getHealthGroupWidth() - 32);
          player.getHealth().getHealthGroup().setTranslateX(player.getHealth().getHealthGroup().getTranslateX() - 1);
        }


        float movementYDivider = (1000f + player.getJumpStrength());
        if (playerAnimation.getTranslateY() > scrollPane.getVvalue() * (1920 - 768f) + 768f - 300f && scrollPane.getVvalue() < 1) {
          scrollPane.setVvalue(scrollPane.getVvalue() + speed / movementYDivider);
          // player.getHealth().getHealthGroup().setTranslateY(scrollPane.getViewportBounds().getMinY() * -1 + 32);
          player.getHealth().getHealthGroup().setTranslateY(player.getHealth().getHealthGroup().getTranslateY() + 1.1745);
        } else {
          player.getHealth().getHealthGroup().setTranslateY(scrollPane.getViewportBounds().getMinY() * -1 + 32);
        }

        if (playerAnimation.getTranslateY() < scrollPane.getVvalue() * (1920 - 768f) + 200f && scrollPane.getVvalue() > 0) {
          scrollPane.setVvalue(scrollPane.getVvalue() - speed / movementYDivider);
          // player.getHealth().getHealthGroup().setTranslateY(scrollPane.getViewportBounds().getMinY() * -1 + 32);
          player.getHealth().getHealthGroup().setTranslateY(player.getHealth().getHealthGroup().getTranslateY() - 1.148);
        } else {
          player.getHealth().getHealthGroup().setTranslateY(scrollPane.getViewportBounds().getMinY() * -1 + 32);
        }

        // System.out.println((scrollPane.getViewportBounds().getMinY() * -1 + 32) + " : " + (scrollPane.getViewportBounds().getMinX() * -1 + 1280 - player.getHealth().getHealthGroupWidth() - 3));

      }
    }));

    scrollTimeline.setCycleCount(Animation.INDEFINITE);
    scrollTimeline.play();


  }



  public AnchorPane getContentPane() {
    return contentPane;
  }

  public ScrollPane getScrollPane() {
    return scrollPane;
  }

  public Timeline getScrollTimeline() {
    return scrollTimeline;
  }

}
