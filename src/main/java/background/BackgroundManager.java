package background;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;

import java.util.ArrayList;

public class BackgroundManager {

  private Scene scene;
  private AnchorPane rootPane;
  private int level;

  private final int CONTENT_WIDTH = 9600;
  private final int CONTENT_HEIGHT = 1920;
  private final int SCROLL_WIDTH = 1280;
  private final int SCROLL_HEIGHT = 768;

  private Background background;
  private AnchorPane contentPane;
  private ScrollPane scrollPane;

  private Line ground;

  private ArrayList<Chunk> chunks;


  public BackgroundManager(Scene scene, AnchorPane rootPane, int level) {

    this.scene = scene;
    this.rootPane = rootPane;
    this.level = level;
    createBackgroundPanes();
    createGround();
    createChunks();
    // createDistanceIndicators();

  }


  private void createBackgroundPanes() {

    background = new Background(level, CONTENT_WIDTH, CONTENT_HEIGHT, SCROLL_WIDTH, SCROLL_HEIGHT);
    contentPane = background.getContentPane();
    scrollPane = background.getScrollPane();

    rootPane.getChildren().add(scrollPane);

  }


  private void createGround() {

    ground = new Line(0, CONTENT_HEIGHT - 128, CONTENT_WIDTH, CONTENT_HEIGHT - 128);
    ground.getStyleClass().add("ground");

    contentPane.getChildren().add(ground);

  }


  private void createChunks() {

    chunks = new ArrayList<>();

    double size = 640;
    for (int y = 0; y < CONTENT_WIDTH / size; y++) {
      for (int x = 0; x < CONTENT_WIDTH / size; x++) {

        Chunk c = new Chunk((int) (x * size), (int) (y * size), size);
        chunks.add(c);
        contentPane.getChildren().add(c.getChunk());

      }

    }

  }








  private void createDistanceIndicators() {

    for (int i = 1; i < 24; i++) {

      Group group = new Group();
      Line line = new Line(i*400, CONTENT_HEIGHT - 100, i*400, CONTENT_HEIGHT - 180);
      line.setStyle("-fx-stroke: rgba(255, 255, 255, .3)");
      Label label = new Label(String.valueOf(i*400));
      label.setTranslateX(i*400);
      label.setTranslateY(CONTENT_HEIGHT - 200);
      label.setStyle("-fx-text-fill: rgba(255, 255, 255, .3)");
      group.getChildren().addAll(line, label);

      contentPane.getChildren().add(group);

    }

  }






  public Background getBackground() {
    return background;
  }

  public Line getGround() {
    return ground;
  }

  public ArrayList<Chunk> getChunks() {
    return chunks;
  }


}
