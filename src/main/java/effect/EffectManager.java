package effect;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

public class EffectManager {

  private AnchorPane contentPane;


  public EffectManager(AnchorPane contentPane) {
    this.contentPane = contentPane;
  }


  public void play(String type, double x, double y) {

    Image spriteImg;
    ImageView imageView;

    switch (type.toLowerCase()) {
      case "enemyhit":

        SoundManager.playSlime();

        spriteImg = new Image("/spritesheet/enemyHit.png");
        imageView = new ImageView(spriteImg);
        createEffect(imageView, x, y, 600, 5, 32, 32);

        break;
      case "playerlanding":

        SoundManager.playStomp();

        spriteImg = new Image("/spritesheet/playerLanding.png");
        imageView = new ImageView(spriteImg);
        createEffect(imageView, x, y, 700, 9, 48, 24);

        break;
    }

  }


  private void createEffect(ImageView imageView, double x, double y, long millis, int count, int width, int height) {

    Pane effectPane = new Pane();
    effectPane.setTranslateX(x);
    effectPane.setTranslateY(y);
    effectPane.setOpacity(.8);

    EffectAnimation effectAnimation = new EffectAnimation(contentPane, effectPane, imageView, millis, count, count, 0, 0, width, height);

    effectPane.getChildren().add(imageView);

    contentPane.getChildren().add(effectPane);

    effectAnimation.play();

  }


}
