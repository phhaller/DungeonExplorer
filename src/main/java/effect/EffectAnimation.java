package effect;

import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.Duration;


public class EffectAnimation extends Transition {

  private ImageView imageView;
  private int count;
  private int columns;
  private int offsetX;
  private int offsetY;
  private int width;
  private int height;

  public EffectAnimation(AnchorPane contentPane, Pane effectPane, ImageView imageView, long millis, int count, int columns, int offsetX, int offsetY, int width, int height) {

    this.imageView = imageView;
    this.count = count;
    this.columns = columns;
    this.offsetX = offsetX;
    this.offsetY = offsetY;
    this.width = width;
    this.height = height;

    setCycleDuration(Duration.millis(millis));
    setCycleCount(1);
    setInterpolator(Interpolator.LINEAR);
    this.imageView.setViewport(new Rectangle2D(offsetX, offsetY, width, height));

    setOnFinished(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        contentPane.getChildren().remove(effectPane);
      }
    });

  }


  public void setOffsetX(int x) {
    this.offsetX = x;
  }

  public void setOffsetY(int y) {
    this.offsetY = y;
  }


  protected void interpolate(double k) {

    final int index = Math.min((int) Math.floor(k * count), count - 1);
    final int x = (index % columns) * width  + offsetX;
    final int y = (index / columns) * height + offsetY;
    imageView.setViewport(new Rectangle2D(x, y, width, height));


  }

}