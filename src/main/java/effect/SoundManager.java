package effect;

import database.SettingsDatabaseManager;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import java.util.ArrayList;

public class SoundManager {

  private static SettingsDatabaseManager settingsDatabaseManager;

  private static MediaPlayer jumpPlayer;
  private static MediaPlayer landingPlayer;
  private static MediaPlayer stompPlayer;
  private static MediaPlayer whipPlayer;
  private static MediaPlayer slimePlayer;
  private static MediaPlayer gatePlayer;
  private static MediaPlayer backgroundMusicPlayer;
  private static ArrayList<MediaPlayer> mediaPlayers;


  public SoundManager() {
    settingsDatabaseManager = new SettingsDatabaseManager();
    createMediaPlayers();
    updateVolumes();
  }


  private void createMediaPlayers() {

    mediaPlayers = new ArrayList<>();

    String jumpURL = getClass().getResource("/sound/jump.mp3").toExternalForm();
    Media jumpMedia = new Media(jumpURL);
    jumpPlayer = new MediaPlayer(jumpMedia);

    String landingURL = getClass().getResource("/sound/landing.mp3").toExternalForm();
    Media landingMedia = new Media(landingURL);
    landingPlayer = new MediaPlayer(landingMedia);

    String stompURL = getClass().getResource("/sound/stomp.mp3").toExternalForm();
    Media stompMedia = new Media(stompURL);
    stompPlayer = new MediaPlayer(stompMedia);

    String whipURL = getClass().getResource("/sound/whip.mp3").toExternalForm();
    Media whipMedia = new Media(whipURL);
    whipPlayer = new MediaPlayer(whipMedia);

    String slimeURL = getClass().getResource("/sound/slime.mp3").toExternalForm();
    Media slimeMedia = new Media(slimeURL);
    slimePlayer = new MediaPlayer(slimeMedia);

    String gateURL = getClass().getResource("/sound/openGate.mp3").toExternalForm();
    Media gateMedia = new Media(gateURL);
    gatePlayer = new MediaPlayer(gateMedia);

    String backgroundMusicURL = getClass().getResource("/sound/backgroundMusic.mp3").toExternalForm();
    Media backgroundMusicMedia = new Media(backgroundMusicURL);
    backgroundMusicPlayer = new MediaPlayer(backgroundMusicMedia);

    // mediaPlayers.add(jumpPlayer);
    // mediaPlayers.add(landingPlayer);
    mediaPlayers.add(stompPlayer);
    mediaPlayers.add(whipPlayer);
    mediaPlayers.add(slimePlayer);
    mediaPlayers.add(gatePlayer);
    mediaPlayers.add(backgroundMusicPlayer);

    playBackgroundMusicPlayer();

  }


  public static void playJump() {
    jumpPlayer.seek(Duration.millis(130));
    jumpPlayer.play();
  }


  public static void playLanding() {
    landingPlayer.seek(Duration.millis(430));
    landingPlayer.play();
  }


  public static void playStomp() {
    stompPlayer.seek(Duration.millis(0));
    stompPlayer.play();
  }


  public static void playWhip() {
    whipPlayer.seek(Duration.millis(0));
    whipPlayer.play();
  }


  public static void playSlime() {
    slimePlayer.seek(Duration.millis(0));
    slimePlayer.play();
  }


  public static void playGate() {
    gatePlayer.seek(Duration.millis(0));
    gatePlayer.play();
  }

  private void playBackgroundMusicPlayer() {
    backgroundMusicPlayer.setCycleCount(MediaPlayer.INDEFINITE);
    backgroundMusicPlayer.play();
  }


  public static void updateVolumes() {
    ArrayList<String> volumes = settingsDatabaseManager.getIdEntry(1);
    String[] vol = volumes.get(0).split("§");
    for (int i = 0; i < mediaPlayers.size(); i++) {
      mediaPlayers.get(i).setVolume(Double.parseDouble(vol[i+1]));
    }
  }


  public static void closeDB() {
    settingsDatabaseManager.closeConnection();
  }


  public ArrayList<MediaPlayer> getMediaPlayers() {
    return mediaPlayers;
  }


}
