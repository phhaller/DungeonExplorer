package inventory;


import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import menu.GameMenu;
import player.Player;
import player.PlayerAnimation;
import utils.CSVReader;

import java.util.Random;

public class Inventory {

  private Scene scene;
  private ScrollPane scrollPane;
  private Player player;

  private AnchorPane inventoryBG;
  private AnchorPane inventory;

  private PlayerAnimation playerAnimation;

  private EventHandler<KeyEvent> eventHandler;

  private int coins = 0;
  private int keys = 0;
  private int slimes = 0;
  private int potionGreen = 0;
  private int potionGreenCollected = 0;
  private int potionBlue = 0;
  private int potionBlueCollected = 0;
  private int potionPurple = 0;
  private int potionPurpleCollected = 0;
  private int potionRed = 0;
  private int potionRedCollected = 0;
  private int potionYellow = 0;
  private int potionYellowCollected = 0;

  private Label heartsLabel;
  private Label coinsLabel;
  private Label keysLabel;
  private Label potionGreenLabel;
  private Label potionBlueLabel;
  private Label potionPurpleLabel;
  private Label potionRedLabel;
  private Label potionYellowLabel;

  private String[][] yellowPotionMap;

  private GameMenu gameMenu;


  public Inventory(Scene scene) {
    this.scene = scene;
    CSVReader reader = new CSVReader("YellowPotionMap.csv");
    yellowPotionMap = reader.getCsvMap();
  }


  private void createInventory() {

    inventoryBG = new AnchorPane();
    inventoryBG.setPrefSize(1280, 768);
    inventoryBG.setTranslateX(0);
    inventoryBG.setTranslateY(1152);
    inventoryBG.getStyleClass().add("inventoryBackground");

    inventory = new AnchorPane();
    inventory.setPrefSize(800, 550);
    inventory.setTranslateX(240);
    inventory.setTranslateY(109);
    inventory.getStyleClass().add("inventory");


    // LEFT SIDE OF INVENTORY
    Image spriteImg = new Image("/spritesheet/inventoryIdle.png");
    ImageView imageView = new ImageView(spriteImg);

    playerAnimation = new PlayerAnimation(imageView, 128, 128);
    playerAnimation.setTranslateX(105);
    playerAnimation.setTranslateY(120);

    Line groundLine = new Line(105, 250, 233, 250);
    groundLine.getStyleClass().add("groundLine");

    Group healthGroup = new Group();
    healthGroup.setTranslateX(50);
    healthGroup.setTranslateY(350);

    Rectangle hearts = new Rectangle(32, 32);
    hearts.getStyleClass().add("healthRec");

    heartsLabel = new Label("3");
    heartsLabel.setPrefSize(70, 32);
    heartsLabel.setTranslateX(210);
    heartsLabel.getStyleClass().add("inventoryAmountLabel");

    healthGroup.getChildren().addAll(hearts, createXLabel(170), heartsLabel);


    Group coinGroup = new Group();
    coinGroup.setTranslateX(50);
    coinGroup.setTranslateY(400);

    Rectangle hearts2 = new Rectangle(32, 32);
    hearts2.getStyleClass().add("coin");

    coinsLabel = new Label(String.valueOf(coins));
    coinsLabel.setPrefSize(70, 32);
    coinsLabel.setTranslateX(210);
    coinsLabel.getStyleClass().add("inventoryAmountLabel");

    coinGroup.getChildren().addAll(hearts2, createXLabel(170), coinsLabel);


    Group keyGroup = new Group();
    keyGroup.setTranslateX(50);
    keyGroup.setTranslateY(450);

    Rectangle hearts3 = new Rectangle(32, 32);
    hearts3.getStyleClass().add("key");

    keysLabel = new Label("0");
    keysLabel.setPrefSize(70, 32);
    keysLabel.setTranslateX(210);
    keysLabel.getStyleClass().add("inventoryAmountLabel");

    keyGroup.getChildren().addAll(hearts3, createXLabel(170), keysLabel);


    // INVENTORY DIVIDER LINE
    Line dividerLine = new Line(320, 30, 320, 520);
    dividerLine.getStyleClass().add("dividerLine");


    // RIGHT SIDE OF INVENTORY
    Group potionGreenGroup = new Group();
    potionGreenGroup.setTranslateX(370);
    potionGreenGroup.setTranslateY(250);

    Rectangle potionG = new Rectangle(32, 32);
    potionG.getStyleClass().add("potionGreen");

    potionGreenLabel = new Label(String.valueOf(potionGreen));
    potionGreenLabel.setPrefSize(70, 32);
    potionGreenLabel.setTranslateX(210);
    potionGreenLabel.getStyleClass().add("inventoryAmountLabel");

    Label usePotionGreen = createUseLabel(275);

    usePotionGreen.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        useGreenPotion();
      }
    });

    potionGreenGroup.getChildren().addAll(potionG, createXLabel(170), potionGreenLabel, usePotionGreen);


    Group potionBlueGroup = new Group();
    potionBlueGroup.setTranslateX(370);
    potionBlueGroup.setTranslateY(300);

    Rectangle potionB = new Rectangle(32, 32);
    potionB.getStyleClass().add("potionBlue");

    potionBlueLabel = new Label(String.valueOf(potionBlue));
    potionBlueLabel.setPrefSize(70, 32);
    potionBlueLabel.setTranslateX(210);
    potionBlueLabel.getStyleClass().add("inventoryAmountLabel");

    Label usePotionBlue = createUseLabel(275);

    usePotionBlue.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        useBluePotion();
      }
    });

    potionBlueGroup.getChildren().addAll(potionB, createXLabel(170), potionBlueLabel, usePotionBlue);


    Group potionPurpleGroup = new Group();
    potionPurpleGroup.setTranslateX(370);
    potionPurpleGroup.setTranslateY(350);

    Rectangle potionP = new Rectangle(32, 32);
    potionP.getStyleClass().add("potionPurple");

    potionPurpleLabel = new Label(String.valueOf(potionPurple));
    potionPurpleLabel.setPrefSize(70, 32);
    potionPurpleLabel.setTranslateX(210);
    potionPurpleLabel.getStyleClass().add("inventoryAmountLabel");

    Label usePotionPurple = createUseLabel(275);

    usePotionPurple.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        usePurplePotion();
      }
    });

    potionPurpleGroup.getChildren().addAll(potionP, createXLabel(170), potionPurpleLabel, usePotionPurple);


    Group potionRedGroup = new Group();
    potionRedGroup.setTranslateX(370);
    potionRedGroup.setTranslateY(400);

    Rectangle potionR = new Rectangle(32, 32);
    potionR.getStyleClass().add("potionRed");

    potionRedLabel = new Label(String.valueOf(potionRed));
    potionRedLabel.setPrefSize(70, 32);
    potionRedLabel.setTranslateX(210);
    potionRedLabel.getStyleClass().add("inventoryAmountLabel");

    Label usePotionRed = createUseLabel(275);

    usePotionRed.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        useRedPotion();
      }
    });

    potionRedGroup.getChildren().addAll(potionR, createXLabel(170), potionRedLabel, usePotionRed);


    Group potionYellowGroup = new Group();
    potionYellowGroup.setTranslateX(370);
    potionYellowGroup.setTranslateY(450);

    Rectangle potionY = new Rectangle(32, 32);
    potionY.getStyleClass().add("potionYellow");

    potionYellowLabel = new Label(String.valueOf(potionYellow));
    potionYellowLabel.setPrefSize(70, 32);
    potionYellowLabel.setTranslateX(210);
    potionYellowLabel.getStyleClass().add("inventoryAmountLabel");

    Label usePotionYellow = createUseLabel(275);

    usePotionYellow.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        useYellowPotion();
      }
    });

    potionYellowGroup.getChildren().addAll(potionY, createXLabel(170), potionYellowLabel, usePotionYellow);


    inventory.getChildren().addAll(playerAnimation, groundLine, healthGroup, coinGroup, keyGroup, dividerLine, potionGreenGroup,
        potionBlueGroup, potionPurpleGroup, potionRedGroup, potionYellowGroup);


    inventoryBG.getChildren().addAll(inventory);

    inventoryBG.setVisible(false);

  }

  private Label createXLabel(int x) {

    Label xLabel = new Label("x");
    xLabel.setPrefSize(32, 32);
    xLabel.setTranslateX(x);
    xLabel.getStyleClass().add("xLabel");

    return xLabel;

  }

  private Label createUseLabel(int x) {

    Label useLabel = new Label("Use");
    useLabel.setPrefSize(90, 32);
    useLabel.setTranslateX(x);
    useLabel.getStyleClass().add("usePotionLabel");

    return useLabel;

  }


  private void createHandlers() {

    eventHandler = new EventHandler<KeyEvent>() {
      @Override
      public void handle(KeyEvent event) {

        if (event.getCode() == KeyCode.I && !gameMenu.getGameMenu().isVisible()) {
          useInventory();
        }

        if (event.getCode() == KeyCode.DIGIT1 && !gameMenu.getGameMenu().isVisible()) {
          useGreenPotion();
        }

        if (event.getCode() == KeyCode.DIGIT2 && !gameMenu.getGameMenu().isVisible()) {
          useBluePotion();
        }

        if (event.getCode() == KeyCode.DIGIT3 && !gameMenu.getGameMenu().isVisible()) {
          usePurplePotion();
        }

        if (event.getCode() == KeyCode.DIGIT4 && !gameMenu.getGameMenu().isVisible()) {
          useRedPotion();
        }

        if (event.getCode() == KeyCode.DIGIT5 && !gameMenu.getGameMenu().isVisible()) {
          useYellowPotion();
        }

      }
    };

    scene.addEventHandler(KeyEvent.KEY_PRESSED, eventHandler);

  }


  public void useInventory() {

    if (inventoryBG.isVisible()) {
      playerAnimation.animation.pause();
      inventoryBG.setVisible(false);
    } else {
      playerAnimation.animation.play();
      inventoryBG.setTranslateX(scrollPane.getViewportBounds().getMinX() * -1);
      inventoryBG.setTranslateY(scrollPane.getViewportBounds().getMinY() * -1);
      heartsLabel.setText(String.valueOf(player.getHealth().getHealthGroup().getChildren().size()));

      inventoryBG.setVisible(true);
    }

    /*System.out.println("Coins: " + coins + "\n" +
        "Keys " + keys + "\n" +
        "PotionGreen " + potionGreen + "\n" +
        "PotionBlue " + potionBlue + "\n" +
        "PotionPurple " + potionPurple + "\n" +
        "PotionRed " + potionRed + "\n" +
        "PotionYellow " + potionYellow
    );*/

  }

  // THE SHIELD IS ACTIVATED WHICH PROTECTS YOU FROM THE NEXT DAMAGE
  private void useGreenPotion() {
    if (potionGreen > 0) {
      if (!player.getShield().isActive()) {
        potionGreen -= 1;
        potionGreenLabel.setText(String.valueOf(potionGreen));
        player.getShield().activate();
      }
    }
  }

  // THE NEXT JUMP WILL BE SUPER HIGH
  private void useBluePotion() {
    if (potionBlue > 0) {
      potionBlue -= 1;
      potionBlueLabel.setText(String.valueOf(potionBlue));
      player.setPotionBlueActive();
    }
  }

  // NOT YET SET
  private void usePurplePotion() {

  }

  // RECEIVE AN EXTRA LIFE
  private void useRedPotion() {
    if (potionRed > 0) {
      player.getHealth().addLife();
      potionRed -= 1;
      potionRedLabel.setText(String.valueOf(potionRed));
      int h = Integer.parseInt(heartsLabel.getText());
      heartsLabel.setText(String.valueOf(h + 1));
    }
  }

  // RECEIVE 3 COINS OR KEYS
  private void useYellowPotion() {

    if (potionYellow > 0) {

      potionYellow -= 1;
      potionYellowLabel.setText(String.valueOf(potionYellow));

      for (int i = 0; i < 3; i++) {

        Random randomX = new Random();
        int randX = (int) randomX.nextInt(10);

        Random randomY = new Random();
        int randY = (int) randomY.nextInt(10);

        System.out.println(randX + " : " + randY);

        String type = yellowPotionMap[randX][randY];
        if (type.contains("Coin")) {
          setCoins(getCoins() + 1);
        }

        if (type.contains("Key")) {
          setKeys(getKeys() + 1);
        }

      }

    }

  }


  public void setScrollPane(ScrollPane scrollPane) {
    this.scrollPane = scrollPane;
  }

  public void setPlayer(Player player) {
    this.player = player;
    createInventory();
    createHandlers();
  }

  public AnchorPane getInventory(){
    return inventoryBG;
  }

  public void setGameMenu(GameMenu gameMenu) {
    this.gameMenu = gameMenu;
  }




  public int getCoins() {
    return coins;
  }

  public void setCoins(int coins) {
    this.coins = coins;
    coinsLabel.setText(String.valueOf(coins));
  }

  public int getKeys() {
    return keys;
  }

  public void setKeys(int keys) {
    this.keys = keys;
    keysLabel.setText(String.valueOf(keys));
  }

  public int getSlimes() {
    return slimes;
  }

  public void addSlime() {
    slimes += 1;
  }

  public int getPotionGreen() {
    return potionGreen;
  }

  public void setPotionGreen(int potionGreen) {
    if (potionGreen > this.potionGreen) {
      potionGreenCollected += 1;
    }
    this.potionGreen = potionGreen;
    potionGreenLabel.setText(String.valueOf(potionGreen));
  }

  public int getPotionGreenCollected() {
    return potionGreenCollected;
  }

  public int getPotionBlue() {
    return potionBlue;
  }

  public void setPotionBlue(int potionBlue) {
    if (potionBlue > this.potionBlue) {
      potionBlueCollected += 1;
    }
    this.potionBlue = potionBlue;
    potionBlueLabel.setText(String.valueOf(potionBlue));
  }

  public int getPotionBlueCollected() {
    return potionBlueCollected;
  }

  public int getPotionPurple() {
    return potionPurple;
  }

  public void setPotionPurple(int potionPurple) {
    if (potionPurple > this.potionPurple) {
      potionPurpleCollected += 1;
    }
    this.potionPurple = potionPurple;
    potionPurpleLabel.setText(String.valueOf(potionPurple));
  }

  public int getPotionPurpleCollected() {
    return potionPurpleCollected;
  }

  public int getPotionRed() {
    return potionRed;
  }

  public void setPotionRed(int potionRed) {
    if (potionRed > this.potionRed) {
      potionRedCollected += 1;
    }
    this.potionRed = potionRed;
    potionRedLabel.setText(String.valueOf(potionRed));
  }

  public int getPotionRedCollected() {
    return potionRedCollected;
  }

  public int getPotionYellow() {
    return potionYellow;
  }

  public void setPotionYellow(int potionYellow) {
    if (potionYellow > this.potionYellow) {
      potionYellowCollected += 1;
    }
    this.potionYellow = potionYellow;
    potionYellowLabel.setText(String.valueOf(potionYellow));
  }

  public int getPotionYellowCollected() {
    return potionYellowCollected;
  }

  public EventHandler<KeyEvent> getEventHandler() {
    return eventHandler;
  }
}
