package obstacle;

import javafx.scene.shape.Rectangle;

public class Obstacle {

  private int x;
  private int y;
  private int width;
  private int height;
  private Rectangle obstacle;
  private String type;
  private boolean hasEnemy = false;

  public Obstacle(int x, int y, int width, int height, String type) {

    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.type = type;

    createObstacle();

  }


  private void createObstacle() {

    obstacle = new Rectangle(width, height);
    obstacle.setTranslateX(x);
    obstacle.setTranslateY(y);
    obstacle.getStyleClass().add("obstacle");

  }


  public void setHasEnemy(boolean hasEnemy) {
    this.hasEnemy = hasEnemy;
  }

  public boolean getHasEnemy() {
    return hasEnemy;
  }

  public Rectangle getObstacle() {
    return obstacle;
  }

  public String getType() {
    return type;
  }
}
