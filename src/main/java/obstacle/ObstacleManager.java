package obstacle;

import utils.CSVReader;

import java.util.ArrayList;

public class ObstacleManager {

  private int level;
  private ArrayList<Obstacle> obstacleList;
  private String[][] obstacleTypes;


  public ObstacleManager(int level) {

    this.level = level;
    obstacleList = new ArrayList<>();
    readObstacles();
    createObstacles();

  }


  private void readObstacles() {
    CSVReader reader = new CSVReader("Obstacles" + level + ".csv");
    obstacleTypes = reader.getCsvMap();
  }



  private void createObstacles() {

    for (int x = 0; x < obstacleTypes.length; x++) {
      for (int y = 0; y < obstacleTypes[0].length; y++) {

        String type = obstacleTypes[x][y];
        int counter = 0;

        if (!type.contains("NULL")) {
          counter++;
          if (type.contains("ground")) {
            int height = 23;

            if (type.contains("Small")) {
              for (int k = x+1; k < 150; k++) {
                if (obstacleTypes[k][y].equals("groundSmall")) {
                  counter++;
                  obstacleTypes[k][y] = "NULL";
                } else {
                  break;
                }
              }
            }

            if (type.contains("Large")) {

              height = 64;
              for (int k = x+1; k < 150; k++) {
                if (obstacleTypes[k][y].equals("groundLarge")) {
                  counter++;
                  obstacleTypes[k][y] = "NULL";
                } else {
                  break;
                }
              }

            }

            if (y != 28) {
              Obstacle obstacle = new Obstacle(x * 64, y * 64, counter * 64, height, "ground");
              obstacleList.add(obstacle);
            }

          }


          if (type.contains("wall")) {
            int width = 64;

            if (type.contains("Regular")) {
              for (int k = y+1; k < 30; k++) {
                if (obstacleTypes[x][k].equals("wallRegular")) {
                  counter++;
                  obstacleTypes[x][k] = "NULL";
                } else {
                  break;
                }
              }

              if (y != 28) {
                Obstacle obstacle = new Obstacle(x * 64, y * 64, width, counter * 64, "wallRegular");
                obstacleList.add(obstacle);
              }

            }

            if (type.contains("Sticky")) {
              for (int k = y+1; k < 30; k++) {
                if (obstacleTypes[x][k].equals("wallSticky")) {
                  counter++;
                  obstacleTypes[x][k] = "NULL";
                } else {
                  break;
                }
              }

              if (y != 28) {
                Obstacle obstacle = new Obstacle(x * 64, y * 64, width, counter * 64, "wallSticky");
                obstacleList.add(obstacle);
              }

            }

          }


          if (type.contains("trap")) {

            for (int k = x+1; k < 150; k++) {
              if (obstacleTypes[k][y].equals("trap")) {
                counter++;
                obstacleTypes[k][y] = "NULL";
              } else {
                break;
              }
            }

            Obstacle obstacle = new Obstacle(x * 64, y * 64 + (64 - 15), counter * 64, 15, "trap");
            obstacleList.add(obstacle);

          }

        }

      }
    }

  }


  public ArrayList<Obstacle> getObstacleList() {
    return obstacleList;
  }

}
