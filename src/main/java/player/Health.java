package player;

import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;
import view.GameOverWindowController;

import java.io.IOException;

public class Health {

  private Scene scene;
  private Group healthGroup;
  private int amount;
  private int width = 32;
  private int height = 32;

  private boolean dead = false;


  public Health(int amount) {

    this.amount = amount;
    createHealth();

  }


  private void createHealth() {

    healthGroup = new Group();
    healthGroup.getStyleClass().add("healthGroup");
    healthGroup.setOpacity(0.0);

    for (int i = 0; i < amount; i++) {
      Rectangle rec = new Rectangle(width, height);
      rec.getStyleClass().add("healthRec");
      rec.setTranslateX(i * width + i * 5);
      healthGroup.getChildren().add(rec);
    }

    healthGroup.setTranslateX(1280 - 32 - amount * width - amount * 5);
    healthGroup.setTranslateY(1920 - 768 + 32);

  }


  public void removeLife() {

    amount -= 1;

    if (amount == 0) {
      dead = true;
    }

    FadeTransition ft = new FadeTransition(Duration.millis(1500), healthGroup);
    ft.setFromValue(0.0);
    ft.setToValue(1.0);
    ft.setCycleCount(1);
    ft.play();

    ft.setOnFinished(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {

        healthGroup.getChildren().remove(amount);

        FadeTransition ft2 = new FadeTransition(Duration.millis(2000), healthGroup);
        ft2.setFromValue(1.0);
        ft2.setToValue(0.0);
        ft2.setCycleCount(1);
        ft2.play();

        healthGroup.setTranslateX(healthGroup.getTranslateX() + 32 + 5);

        if (dead) {
          System.out.println("GAME OVER");
          switchScene();
        }

      }
    });

  }


  public void addLife() {

    FadeTransition ft = new FadeTransition(Duration.millis(1500), healthGroup);
    ft.setFromValue(0.0);
    ft.setToValue(1.0);
    ft.setCycleCount(1);
    ft.play();

    ft.setOnFinished(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {

        Rectangle rec = new Rectangle(width, height);
        rec.getStyleClass().add("healthRec");
        rec.setTranslateX(amount * width + amount * 5);
        healthGroup.getChildren().add(rec);
        amount += 1;

        FadeTransition ft2 = new FadeTransition(Duration.millis(2000), healthGroup);
        ft2.setFromValue(1.0);
        ft2.setToValue(0.0);
        ft2.setCycleCount(1);
        ft2.play();

        healthGroup.setTranslateX(healthGroup.getTranslateX() - 32 - 5);

      }
    });

  }


  private void switchScene() {

    try {
      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/gameOverWindow.fxml"));
      Parent root = loader.load();
      Stage stage = (Stage) healthGroup.getScene().getWindow();

      GameOverWindowController controller = loader.getController();
      controller.setScene(scene);

      stage.getScene().setRoot(root);

    } catch (IOException e) {
      e.printStackTrace();
    }

  }



  public Group getHealthGroup() {
    return healthGroup;
  }

  public int getHealthGroupWidth() {
    return healthGroup.getChildren().size() * width + healthGroup.getChildren().size() * 5;
  }

  public boolean isDead() {
    return dead;
  }

  public void setScene(Scene scene) {
    this.scene = scene;
  }

  public int getAmount() {
    return amount;
  }

}
