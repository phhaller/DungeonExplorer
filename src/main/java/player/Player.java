package player;

import collision.CollisionManager;
import effect.EffectManager;
import effect.SoundManager;
import inventory.Inventory;
import javafx.animation.Animation;
import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import menu.GameMenu;
import obstacle.Obstacle;
import utils.SpriteAnimation;

public class Player {

  private Scene scene;

  private int x;
  private int y;
  private Rectangle hitbox;
  private PlayerAnimation playerAnimation;

  private double moveSpeed = .2;

  private CollisionManager collisionManager;
  private EffectManager effectManager;

  private Health health;
  private Shield shield;

  private Timeline moveTimeline;
  private EventHandler<KeyEvent> eventPressedHandler;
  private EventHandler<KeyEvent> eventReleasedHandler;

  private boolean LEFT = false;
  private boolean RIGHT = false;
  private boolean JUMPING = false;
  private boolean ON_OBSTACLE = false;
  private boolean WALL_SLIDING = false;
  private boolean FALLING = false;
  private String currentDir = "RIGHT";

  private float jumpStrength = 18;
  private float weight = .9f;
  private float RESET_JUMPSTRENGTH = 18;
  private float RESET_WEIGHT = .9f;

  private double fallStrength = 5;
  private final double RESET_FALLSTRENGTH = 2;
  private double fallWeight = .9f;
  private final double RESET_FALLWEIGHT = 1;

  private Obstacle currentSlidingObstacle = null;
  private int slidingJumpCounter = 0;

  private Inventory inventory;
  private GameMenu gameMenu;

  private boolean potionBlueActive = false;


  public Player(Scene scene, int x, int y) {

    this.scene = scene;
    this.x = x;
    this.y = y;

    collisionManager = new CollisionManager();

    createPlayer();
    createHealth();
    createShield();
    createHandlers();
    createMovement();

  }


  private void createPlayer() {

    Image spriteImg = new Image("/spritesheet/SpritesheetV3.png");
    ImageView imageView = new ImageView(spriteImg);

    playerAnimation = new PlayerAnimation(imageView, 64, 64);
    playerAnimation.setTranslateX(x);
    playerAnimation.setTranslateY(y);

    hitbox = playerAnimation.getHitBox();
    hitbox.translateXProperty().bind(playerAnimation.translateXProperty());
    hitbox.translateYProperty().bind(playerAnimation.translateYProperty());
    hitbox.getStyleClass().add("player");

  }


  private void createHealth() {
    health = new Health(3);
    health.setScene(scene);
  }


  private void createShield() {
    shield = new Shield();
    Circle shieldCircle = shield.getShield();
    shieldCircle.translateXProperty().bind(playerAnimation.translateXProperty().add(32));
    shieldCircle.translateYProperty().bind(playerAnimation.translateYProperty().add(32));
  }


  private void createHandlers() {

    eventPressedHandler = new EventHandler<KeyEvent>() {
      @Override
      public void handle(KeyEvent event) {

        switch (event.getCode()) {

          case A:
            if (!inventory.getInventory().isVisible() && !gameMenu.getGameMenu().isVisible()) {
              LEFT = true;
            }
            break;
          case D:
            if (!inventory.getInventory().isVisible() && !gameMenu.getGameMenu().isVisible()) {
              RIGHT = true;
            }
            break;
          case SPACE:
            if (!inventory.getInventory().isVisible() && !gameMenu.getGameMenu().isVisible()) {
              jump();
            }
            break;

        }

      }
    };

    scene.addEventHandler(KeyEvent.KEY_PRESSED, eventPressedHandler);


    eventReleasedHandler = new EventHandler<KeyEvent>() {
      @Override
      public void handle(KeyEvent event) {

        switch (event.getCode()) {

          case A: LEFT = false; break;
          case D: RIGHT = false; break;

        }

      }
    };

    scene.addEventHandler(KeyEvent.KEY_RELEASED, eventReleasedHandler);

  }


  private void createMovement() {

    moveTimeline = new Timeline(new KeyFrame(Duration.millis(1), new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {

        collisionManager.checkCurrentChunk();

        if (LEFT && !collisionManager.checkWindowBorder()) {
          if (!collisionManager.checkSideCollision(moveSpeed, "LEFT", ON_OBSTACLE)) {
            checkWalkingOff();
            collisionManager.checkCollectibleCollision();
            currentDir = "LEFT";
            playerAnimation.setTranslateX(playerAnimation.getTranslateX() - moveSpeed);
            playerAnimation.animation.play();
            playerAnimation.animation.setOffsetY(64);
          } else {
            if (JUMPING && jumpStrength < 0 && !WALL_SLIDING &&
                collisionManager.getObstacleType().equals("wallSticky")) {
              slide("LEFT");
            }
          }
        } else if (RIGHT && !collisionManager.checkWindowBorder()) {
          if (!collisionManager.checkSideCollision(moveSpeed, "RIGHT", ON_OBSTACLE)) {
            checkWalkingOff();
            collisionManager.checkCollectibleCollision();
            currentDir = "RIGHT";
            playerAnimation.setTranslateX(playerAnimation.getTranslateX() + moveSpeed);
            playerAnimation.animation.play();
            playerAnimation.animation.setOffsetY(0);
          } else {
            if (JUMPING && jumpStrength < 0 && !WALL_SLIDING &&
                collisionManager.getObstacleType().equals("wallSticky")) {
              slide("RIGHT");
            }
          }
        } else {
          if (currentDir.equals("RIGHT")) {
            playerAnimation.animation.play();
            playerAnimation.animation.setOffsetY(129);
          } else {
            playerAnimation.animation.play();
            playerAnimation.animation.setOffsetY(193);
          }

          // playerAnimation.animation.stop();
        }

      }
    }));

    moveTimeline.setCycleCount(Animation.INDEFINITE);
    moveTimeline.play();

  }


  private void jump() {

    if ((collisionManager.checkGround() || ON_OBSTACLE || WALL_SLIDING) && slidingJumpCounter < 3) {

      SoundManager.playJump();

      JUMPING = true;
      ON_OBSTACLE = false;
      WALL_SLIDING = false;

      if (!potionBlueActive) {
        jumpStrength = RESET_JUMPSTRENGTH;
      } else {
        jumpStrength = 30;
        potionBlueActive = false;
      }
      weight = RESET_WEIGHT;

      new AnimationTimer() {
          @Override
          public void handle(long now) {

            playerAnimation.setTranslateY(playerAnimation.getTranslateY() - jumpStrength);
            collisionManager.checkCollectibleCollision();

            jumpStrength -= weight;
            if (jumpStrength < -20) {
              jumpStrength = -20;
            }

            String collision = collisionManager.checkObstacle(jumpStrength);
            if (!collision.isEmpty()) {
              if (collision.equals("BOTTOM")) {
                jumpStrength = 0;
                weight = RESET_WEIGHT;
              } else if (collision.equals("TOP")) {
                createLandingEffect();
                weight = RESET_WEIGHT;
                jumpStrength = RESET_JUMPSTRENGTH;
                JUMPING = false;
                ON_OBSTACLE = true;
                slidingJumpCounter = 0;
                stop();
              }
            }

            if (collisionManager.checkGround()) {
              createLandingEffect();
              jumpStrength = RESET_JUMPSTRENGTH;
              weight = RESET_WEIGHT;
              JUMPING = false;
              slidingJumpCounter = 0;
              stop();
            }

            if (WALL_SLIDING) {
              stop();
            }

          }
        }.start();

        slidingJumpCounter++;

    }

  }


  private void slide(String dir) {

    WALL_SLIDING = true;
    JUMPING = false;
    weight = .1f;

    new AnimationTimer() {
      @Override
      public void handle(long now) {

        playerAnimation.setTranslateY(playerAnimation.getTranslateY() - jumpStrength);
        collisionManager.checkCollectibleCollision();

        jumpStrength -= weight;
        if(jumpStrength < -20) {
          jumpStrength = -20;
        }

        if (dir.equals("LEFT")) {
          if (RIGHT) {
            weight = RESET_WEIGHT;
            WALL_SLIDING = false;
          }
        }

        if (dir.equals("RIGHT")) {
          if (LEFT) {
            weight = RESET_WEIGHT;
            WALL_SLIDING = false;
          }
        }

        if (JUMPING) {
          WALL_SLIDING = false;
          stop();
        }

        if (collisionManager.checkGround()) {
          createLandingEffect();
          jumpStrength = RESET_JUMPSTRENGTH;
          weight = RESET_WEIGHT;
          WALL_SLIDING = false;
          slidingJumpCounter = 0;
          stop();
        }

        String collision = collisionManager.checkObstacle(jumpStrength);
        if (!collision.isEmpty()) {
          if (collision.equals("TOP")) {
            createLandingEffect();
            jumpStrength = RESET_JUMPSTRENGTH;
            weight = RESET_WEIGHT;
            WALL_SLIDING = false;
            ON_OBSTACLE = true;
            slidingJumpCounter = 0;
            stop();
          }
        }


      }
    }.start();

  }


  private void checkWalkingOff() {

    if (ON_OBSTACLE && !JUMPING) {

      if (collisionManager.isWalkingOff()) {

        ON_OBSTACLE = false;
        FALLING = true;
        jumpStrength = -19;

        new AnimationTimer() {
          @Override
          public void handle(long now) {

            playerAnimation.setTranslateY(playerAnimation.getTranslateY() + fallStrength);
            collisionManager.checkCollectibleCollision();

            String collision = collisionManager.checkObstacle(jumpStrength);
            if (!collision.isEmpty()) {
              if (collision.equals("TOP")) {
                createLandingEffect();
                fallStrength = RESET_FALLSTRENGTH;
                jumpStrength = RESET_JUMPSTRENGTH;
                weight = RESET_WEIGHT;
                ON_OBSTACLE = true;
                FALLING = false;
                stop();
              }
            }

            if (collisionManager.checkGround()) {
              createLandingEffect();
              fallStrength = RESET_FALLSTRENGTH;
              jumpStrength = RESET_JUMPSTRENGTH;
              weight = RESET_WEIGHT;
              FALLING = false;
              stop();
            }

            fallStrength += fallWeight;
            if(fallStrength > 20) {
              fallStrength = 20;
            }

          }
        }.start();

      }

    }

  }


  private void createLandingEffect() {

    if (jumpStrength <= -20 || fallStrength >= 20) {
      effectManager.play("playerLanding", playerAnimation.getTranslateX() + 8, playerAnimation.getTranslateY() + 53);
    } else {
      SoundManager.playLanding();
    }

  }





  public void setCollisionManager(CollisionManager collisionManager) {
    this.collisionManager = collisionManager;
  }

  public CollisionManager getCollisionManager() {
    return collisionManager;
  }

  public void setEffectManager(EffectManager effectManager) {
    this.effectManager = effectManager;
  }

  public Rectangle getHitbox() {
    return hitbox;
  }

  public PlayerAnimation getPlayerAnimation() {
    return playerAnimation;
  }

  public String getCurrentDir() {
    return currentDir;
  }

  public void setPotionBlueActive() {
    potionBlueActive = true;
  }

  public float getJumpStrength() {
    return jumpStrength;
  }

  public Health getHealth() {
    return health;
  }

  public Shield getShield() {
    return shield;
  }

  public void setInventory(Inventory inventory) {
    this.inventory = inventory;
  }

  public Inventory getInventory() {
    return inventory;
  }

  public void setGameMenu(GameMenu gameMenu) {
    this.gameMenu = gameMenu;
  }

  public Timeline getMoveTimeline() {
    return moveTimeline;
  }

  public SpriteAnimation getAnimation() {
    return playerAnimation.animation;
  }

  public EventHandler<KeyEvent> getEventPressedHandler() {
    return eventPressedHandler;
  }

  public EventHandler<KeyEvent> getEventReleasedHandler() {
    return eventReleasedHandler;
  }

}
