package player;

import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;

public class Shield {

  private Circle shield;
  private boolean shieldActive = false;
  private FadeTransition fadeTransition;

  public Shield() {
    createShield();
  }


  private void createShield() {

    shield = new Circle();
    shield.setRadius(64);
    shield.getStyleClass().add("shield");

    DropShadow borderGlow = new DropShadow();
    borderGlow.setColor(Color.WHITE);
    borderGlow.setOffsetX(0f);
    borderGlow.setOffsetY(0f);
    borderGlow.setWidth(20);
    borderGlow.setHeight(20);

    shield.setEffect(borderGlow);

    fadeTransition = new FadeTransition(Duration.millis(1200), shield);
    fadeTransition.setFromValue(.9);
    fadeTransition.setToValue(0.4);
    fadeTransition.setCycleCount(Animation.INDEFINITE);
    fadeTransition.setAutoReverse(true);

    fadeTransition.play();

    shield.setVisible(false);

  }


  public void activate() {
    shieldActive = true;
    fadeTransition.play();
    shield.setVisible(true);
  }


  public void deactivate() {
    shieldActive = false;
    fadeTransition.pause();
    shield.setVisible(false);
  }





  public boolean isActive() {
    return shieldActive;
  }

  public Circle getShield() {
    return shield;
  }

}
