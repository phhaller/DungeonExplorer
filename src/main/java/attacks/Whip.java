package attacks;

import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

public class Whip extends Transition {


  private final ImageView imageView;

  private final int count = 4;
  private final int columns = 4;
  private int offsetX = 0;
  private int offsetY = 0;
  private final int width = 80;
  private final int height = 40;

  public Whip(ImageView imageView, Pane slingShotPane) {

    this.imageView = imageView;

    setCycleDuration(Duration.millis(350));
    setCycleCount(1);
    setInterpolator(Interpolator.LINEAR);

    setOnFinished(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        slingShotPane.setVisible(false);
      }
    });

    this.imageView.setViewport(new Rectangle2D(offsetX, offsetY, width, height));

  }


  public void setOffsetX(int x) {
    this.offsetX = x;
  }

  public void setOffsetY(int y) {
    this.offsetY = y;
  }


  protected void interpolate(double k) {

    final int index = Math.min((int) Math.floor(k * count), count - 1);
    final int x = (index % columns) * width  + offsetX;
    final int y = (index / columns) * height + offsetY;
    imageView.setViewport(new Rectangle2D(x, y, width, height));


  }


}
