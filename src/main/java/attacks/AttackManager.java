package attacks;

import collision.CollisionManager;
import effect.SoundManager;
import enemy.Enemy;
import javafx.animation.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.Glow;
import javafx.scene.effect.Shadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.util.Duration;
import menu.GameMenu;
import player.Player;

import java.util.ArrayList;

public class AttackManager {

  private AnchorPane contentPane;
  private Player player;

  private Pane whipPane;
  private Whip whip;

  private Line fireBeam;

  private CollisionManager collisionManager;
  private GameMenu gameMenu;


  public AttackManager(AnchorPane contentPane, Player player) {

    this.contentPane = contentPane;
    this.player = player;
    createWhip();

  }


  private void createWhip() {

    whipPane = new AnchorPane();
    whipPane.getStyleClass().add("attackPane");

    Image spriteImg = new Image("/spritesheet/whip.png");
    ImageView imageView = new ImageView(spriteImg);

    whip = new Whip(imageView, whipPane);

    whipPane.getChildren().add(imageView);
    whipPane.setVisible(false);
    contentPane.getChildren().add(whipPane);

  }


  public void createFireBeam(double startX, double startY, String direction) {

    System.out.println(startX + " : " + startY + " : " + player.getPlayerAnimation().getTranslateX() + " : " + player.getPlayerAnimation().getTranslateY());

    fireBeam = new Line();
    fireBeam.setStartX(startX);
    fireBeam.setStartY(startY);
    fireBeam.setEndX(player.getPlayerAnimation().getTranslateX() + 32);
    fireBeam.setEndY(player.getPlayerAnimation().getTranslateY() + 32);

    fireBeam.setOpacity(0.0);
    fireBeam.getStyleClass().add("fireBeam");

    contentPane.getChildren().add(fireBeam);

    Glow glow = new Glow(.4);
    fireBeam.setEffect(glow);

    Shadow shadow = new Shadow();
    shadow.setBlurType(BlurType.GAUSSIAN);
    shadow.setColor(Color.rgb(255, 255, 255, .3));
    shadow.setHeight(5);
    shadow.setWidth(5);
    shadow.setRadius(7);
    fireBeam.setEffect(shadow);

    FadeTransition ft = new FadeTransition(Duration.millis(600), fireBeam);
    ft.setFromValue(0.0);
    ft.setToValue(1.0);
    ft.setAutoReverse(true);
    ft.setCycleCount(2);
    ft.setInterpolator(Interpolator.EASE_IN);

    ft.play();

    ft.setOnFinished(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {
        contentPane.getChildren().remove(fireBeam);
      }
    });

  }


  public void createSingleShot(int x, int y) {

    Shot shot = new Shot(x, y, (int) player.getPlayerAnimation().getTranslateX() + 32,
        (int) player.getPlayerAnimation().getTranslateY() + 32, collisionManager, contentPane);

  }



  private void createHandlers() {

    contentPane.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {

        if (!player.getInventory().getInventory().isVisible() && !gameMenu.getGameMenu().isVisible() && !whipPane.isVisible()) {

          whipPane.setVisible(true);
          if (player.getCurrentDir().equals("RIGHT")) {
            whipPane.setTranslateX(player.getPlayerAnimation().getTranslateX() + 64);
            whip.setOffsetY(40);
          } else {
            whipPane.setTranslateX(player.getPlayerAnimation().getTranslateX() - 80);
            whip.setOffsetY(0);
          }
          whipPane.setTranslateY(player.getPlayerAnimation().getTranslateY() + 24);

          SoundManager.playWhip();
          whip.play();

          ArrayList<Enemy> enemyList = collisionManager.checkSlingShotEnemyCollision();
          if (!enemyList.isEmpty()) {

            for (Enemy e : enemyList) {
              e.kill(contentPane);
              player.getInventory().addSlime();
            }

          }

        }

      }
    });

  }


  public void setCollisionManager(CollisionManager collisionManager) {
    this.collisionManager = collisionManager;
  }


  public void setGameMenu(GameMenu gameMenu) {
    this.gameMenu = gameMenu;
    createHandlers();
  }

  public Pane getWhipPane() {
    return whipPane;
  }

}
