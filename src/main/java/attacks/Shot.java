package attacks;

import collision.CollisionManager;
import javafx.animation.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;

public class Shot {

  private int startX;
  private int startY;
  private int angle;
  private int length;
  private int endX;
  private int endY;
  private CollisionManager collisionManager;
  private AnchorPane contentPane;
  private Circle shot;
  private Timeline timeline;
  private SequentialTransition effectTransition;

  private int shotCounter = 1;
  private double shotSpeedMultiplier = 1.0;


  public Shot(int startX, int startY, int endX, int endY, CollisionManager collisionManager, AnchorPane contentPane) {

    this.startX = startX;
    this.startY = startY;
    this.endX = endX;
    this.endY = endY;
    this.collisionManager = collisionManager;
    this.contentPane = contentPane;

    createShot();
    createEffect();
    createMovement();

  }


  private void createShot() {

    shot = new Circle();
    shot.setRadius(7);
    shot.setCenterX(0);
    shot.setCenterY(0);
    shot.setTranslateX(startX);
    shot.setTranslateY(startY);

    contentPane.getChildren().add(shot);

  }


  private void createEffect() {

    FillTransition ft = new FillTransition();
    ft.setShape(shot);
    ft.setDuration(Duration.millis(100));
    ft.setFromValue(Color.web("#EDBE57"));
    ft.setToValue(Color.web("#D06049"));
    /*ft.setFromValue(Color.DARKGREY);
    ft.setToValue(Color.GREY);*/
    ft.setAutoReverse(true);
    ft.setCycleCount(Animation.INDEFINITE);

    FadeTransition fat = new FadeTransition();
    fat.setNode(shot);
    fat.setDuration(Duration.millis(100));
    fat.setFromValue(1.0);
    fat.setToValue(0.4);
    fat.setAutoReverse(true);
    fat.setCycleCount(Animation.INDEFINITE);

    effectTransition = new SequentialTransition(ft, fat);
    effectTransition.play();

  }


  private void createMovement() {

    length = (int) Math.sqrt(Math.pow(startX - endX, 2) + Math.pow(startY - endY, 2));

    int dx = endX - startX;
    int dy = startY - endY;
    angle = (int) Math.toDegrees(Math.atan2(dx, dy));
    angle = angle < 0 ? angle += 360 : angle;

    timeline = new Timeline(new KeyFrame(Duration.millis(5), new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent event) {

        if (shotCounter < length && !collisionManager.checkShotPlayerIntersection(shot)) {

          shot.setTranslateX(startX + (shotCounter * Math.sin(Math.toRadians(angle))) * shotSpeedMultiplier);
          shot.setTranslateY(startY - (shotCounter * Math.cos(Math.toRadians(angle))) * shotSpeedMultiplier);

          if (shotSpeedMultiplier < 3) {
            shotSpeedMultiplier += .01;
          }

          shotCounter++;

        } else {
          timeline.stop();
          effectTransition.stop();
          contentPane.getChildren().remove(shot);
        }

      }
    }));

    timeline.setCycleCount(Animation.INDEFINITE);
    timeline.play();

  }

}
