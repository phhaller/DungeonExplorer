package exit;

import effect.SoundManager;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import player.Player;
import utils.AnimationEventManager;
import utils.CSVReader;
import view.WinWindowController;

import java.io.IOException;

public class ExitManager {

  private Scene scene;
  private AnchorPane contentPane;
  private Player player;
  private int level;
  private String[][] exitTypes;

  private AnimationEventManager animationEventManager;

  private Exit exit = null;
  private EventHandler<KeyEvent> eventHandler = null;


  public ExitManager(Scene scene, AnchorPane contentPane, int level) {

    this.scene = scene;
    this.contentPane = contentPane;
    this.level = level;

    readExit();
    createExit();

  }


  private void readExit() {
    CSVReader reader = new CSVReader("Exit" + level + ".csv");
    exitTypes = reader.getCsvMap();
  }


  private void createExit() {

    for (int x = 0; x < exitTypes.length; x++) {
      for (int y = 0; y < exitTypes[0].length; y++) {

        String type = exitTypes[x][y];
        if (type.contains("exit")) {

          int width = 100;
          int height = 128;

          exit = new Exit(x * 64, y * 64 - 64, width, height, "exit");

        }

      }
    }

  }


  private void createHandler() {

    eventHandler = new EventHandler<KeyEvent>() {
      @Override
      public void handle(KeyEvent event) {
        if (event.getCode() == KeyCode.E) {

          if (player.getPlayerAnimation().getBoundsInParent().intersects(exit.getExitPane().getBoundsInParent()) &&
              player.getInventory().getKeys() > 0 && !exit.isOpen()) {
            player.getInventory().setKeys(player.getInventory().getKeys() - 1);
            SoundManager.playGate();
            exit.playAnimation();
            exit.setOpen(true);
          }

        }

        if (event.getCode() == KeyCode.W) {

          if (player.getPlayerAnimation().getBoundsInParent().intersects(exit.getExitPane().getBoundsInParent()) &&
              exit.isOpen()) {
            System.out.println("YOU WON!");
            animationEventManager.stopGame();
            contentPane.getChildren().remove(exit.getExitPane());
            switchScene();
          }

        }
      }
    };

    scene.addEventHandler(KeyEvent.KEY_PRESSED, eventHandler);

  }


  private void switchScene() {

    SoundManager.closeDB();

    try {
      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/winWindow.fxml"));
      Parent root = loader.load();
      Stage stage = (Stage) scene.getWindow();

      WinWindowController controller = loader.getController();
      controller.setScene(scene);
      controller.setLevel(level);
      controller.setStats(player.getHealth().getAmount(),
                          player.getInventory().getCoins(),
                          player.getInventory().getKeys(),
                          player.getInventory().getSlimes(),
                          player.getInventory().getPotionGreenCollected(),
                          player.getInventory().getPotionBlueCollected(),
                          player.getInventory().getPotionPurpleCollected(),
                          player.getInventory().getPotionRedCollected(),
                          player.getInventory().getPotionYellowCollected());

      stage.getScene().setRoot(root);

    } catch (IOException e) {
      e.printStackTrace();
    }

  }





  public void setPlayer(Player player) {
    this.player = player;
    createHandler();
  }


  public void setAnimationEventManager(AnimationEventManager animationEventManager) {
    this.animationEventManager = animationEventManager;
  }

  public Exit getExit() {
    return exit;
  }

  public EventHandler<KeyEvent> getEventHandler() {
    return eventHandler;
  }
}
