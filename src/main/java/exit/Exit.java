package exit;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class Exit {

  private int x;
  private int y;
  private int width;
  private int height;
  private Pane exitPane;
  private ExitAnimation exitAnimation;
  private String type;
  private boolean open = false;

  public Exit(int x, int y, int width, int height, String type) {

    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.type = type;

    createExit();

  }


  private void createExit() {

    Image spriteImg = new Image("/spritesheet/door.png");
    ImageView imageView = new ImageView(spriteImg);

    exitPane = new Pane();
    exitPane.setTranslateX(x);
    exitPane.setTranslateY(y);

    exitAnimation = new ExitAnimation(imageView, 700, 8, 8, 0, 0, width, height);

    exitPane.getChildren().add(imageView);

  }


  public void playAnimation() {
    exitAnimation.play();
  }


  public Pane getExitPane() {
    return exitPane;
  }

  public String getType() {
    return type;
  }

  public boolean isOpen() {
    return open;
  }

  public void setOpen(boolean open) {
    this.open = open;
  }

}
