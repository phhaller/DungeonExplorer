package view;

import database.HighscoreDatabaseManager;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import menu.HelpMenu;
import menu.LevelMenu;
import menu.LoadingScreen;
import menu.SettingsMenu;

import java.io.IOException;

public class StartWindowController {

  @FXML
  private AnchorPane rootPane;

  @FXML
  private Label playButton;

  @FXML
  private Label levelsLabel;

  @FXML
  private Label settingsLabel;

  @FXML
  private Label helpLabel;


  private Scene scene;
  private LevelMenu levelMenu;
  private SettingsMenu settingsMenu;
  private HelpMenu helpMenu;

  private HighscoreDatabaseManager highscoreDatabaseManager;
  private int maxLevel = 0;



  private void initialize() {

    createDatabase();
    createMenus();
    createHandler();

  }


  private void createDatabase() {
    highscoreDatabaseManager = new HighscoreDatabaseManager();
    int max = highscoreDatabaseManager.getMaxLevel() + 1;
    if (max > maxLevel) {
      maxLevel = max;
    }
  }


  private void createMenus() {
    levelMenu = new LevelMenu(scene, highscoreDatabaseManager);
    settingsMenu = new SettingsMenu("start");
    helpMenu = new HelpMenu();

    rootPane.getChildren().addAll(levelMenu.getLevelMenuPane(), settingsMenu.getSettingsMenu(), helpMenu.getHelpMenu());
  }


  private void createHandler() {

    playButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        switchScene(maxLevel);
        levelsLabel.setVisible(false);
        settingsLabel.setVisible(false);
        helpLabel.setVisible(false);
      }
    });


    levelsLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        System.out.println("Levels clicked");
        levelMenu.setMaxLevel(maxLevel);
        levelMenu.getLevelMenuPane().setVisible(true);
      }
    });


    settingsLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        System.out.println("Settings clicked");
        settingsMenu.setVisible();
      }
    });


    helpLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        System.out.println("Help clicked");
        helpMenu.getHelpMenu().setVisible(true);
      }
    });

  }



  private void switchScene(int level) {

    highscoreDatabaseManager.closeConnection();

    LoadingScreen loadingScreen = new LoadingScreen(level);
    rootPane.getChildren().add(loadingScreen.getLoadingScreen());

    try {

      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/gameWindow.fxml"));
      Parent root = loader.load();
      Stage stage = (Stage) rootPane.getScene().getWindow();

      GameWindowController controller = loader.getController();
      controller.setScene(scene);
      controller.setLevel(level);

      loadingScreen.getLoadingScreen().setVisible(true);

      FadeTransition ft = new FadeTransition(Duration.millis(2000), loadingScreen.getLevelLabel());
      ft.setFromValue(0.0);
      ft.setToValue(1.0);
      ft.setCycleCount(1);
      ft.setAutoReverse(true);

      ft.play();

      ft.setOnFinished(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
          stage.getScene().setRoot(root);

        }
      });


    } catch (IOException e) {
      e.printStackTrace();
    }

  }





  public void setScene(Scene scene) {
    this.scene = scene;
    initialize();
  }

}
