package view;

import attacks.AttackManager;
import background.BackgroundManager;
import background.Chunk;
import collectible.Collectible;
import collectible.CollectiblesManager;
import collision.CollisionManager;
import effect.EffectManager;
import effect.SoundManager;
import enemy.Enemy;
import enemy.EnemyManager;
import exit.ExitManager;
import inventory.Inventory;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import menu.GameMenu;
import obstacle.Obstacle;
import obstacle.ObstacleManager;
import player.Player;
import player.PlayerAnimation;
import utils.AnimationEventManager;

import java.util.ArrayList;

public class GameWindowController {

  @FXML
  private AnchorPane rootPane;

  private Scene scene;
  private int level;

  private AnimationEventManager animationEventManager;

  private CollisionManager collisionManager;

  private BackgroundManager backgroundManager;

  private EffectManager effectManager;
  private SoundManager soundManager;

  private EnemyManager enemyManager;

  private Player player;
  private Rectangle playerHitbox;
  private PlayerAnimation playerAnimation;

  private AttackManager attackManager;

  private ObstacleManager obstacleManager;
  private ArrayList<Obstacle> obstacleList;

  private ExitManager exitManager;

  private CollectiblesManager collectiblesManager;
  private ArrayList<Collectible> collectibles;

  private Inventory inventory;
  private GameMenu gameMenu;


  private void initialize() {

    createAnimationEventManager();
    createCollisionManager();
    createBackground();
    createEffects();
    createObstacles();
    createExit();
    createCollectibles();
    createPlayer();
    createAttackManager();
    createEnemies();
    createInventory();
    createGameMenu();

  }


  private void createAnimationEventManager() {
    animationEventManager = new AnimationEventManager(scene);
  }


  private void createCollisionManager() {
    collisionManager = new CollisionManager();
    collisionManager.setAnimationEventManager(animationEventManager);
  }


  private void createBackground() {
    backgroundManager = new BackgroundManager(scene, rootPane, level);
    collisionManager.setGround(backgroundManager.getGround());

  }


  private void createEffects() {
    effectManager = new EffectManager(backgroundManager.getBackground().getContentPane());
    soundManager = new SoundManager();
    animationEventManager.addMediaPlayers(soundManager.getMediaPlayers());
  }


  private void createObstacles() {

    obstacleManager = new ObstacleManager(level);
    obstacleList = obstacleManager.getObstacleList();

    for (Obstacle o : obstacleList) {
      backgroundManager.getBackground().getContentPane().getChildren().add(o.getObstacle());
    }

    for (Chunk c : backgroundManager.getChunks()) {
      for (Obstacle o : obstacleList) {

        if (c.getChunk().getBoundsInParent().intersects(o.getObstacle().getBoundsInParent())) {
          c.addObstacleToChunk(o);
        }

      }

    }

    collisionManager.setChunks(backgroundManager.getChunks());

  }


  private void createExit() {

    exitManager = new ExitManager(scene, backgroundManager.getBackground().getContentPane(), level);
    exitManager.setAnimationEventManager(animationEventManager);

    if (exitManager.getExit() != null) {
      backgroundManager.getBackground().getContentPane().getChildren().add(exitManager.getExit().getExitPane());
    }

  }


  private void createCollectibles() {

    collectiblesManager = new CollectiblesManager(backgroundManager.getBackground().getContentPane(), level);
    collectibles = collectiblesManager.getCollectibles();

    for (Collectible c : collectibles) {
      backgroundManager.getBackground().getContentPane().getChildren().add(c.getCollectible());
      c.setEffectManager(effectManager);
    }

    for (Chunk c : backgroundManager.getChunks()) {
      for (Collectible co : collectibles) {

        if (c.getChunk().getBoundsInParent().intersects(co.getCollectible().getBoundsInParent())) {
          c.addCollectibleToChunk(co);
        }

      }
    }

    collisionManager.setChunks(backgroundManager.getChunks());

  }


  private void createPlayer() {

    // player = new Player(scene, 200, 9408);
    player = new Player(scene, 200, 1728);
    playerHitbox = player.getHitbox();
    playerAnimation = player.getPlayerAnimation();

    backgroundManager.getBackground().getContentPane().getChildren().addAll(playerAnimation, playerHitbox,
        player.getHealth().getHealthGroup(), player.getShield().getShield());
    backgroundManager.getBackground().setPlayer(player);

    player.setCollisionManager(collisionManager);
    player.setEffectManager(effectManager);
    collisionManager.setPlayer(player);
    collisionManager.setPlayerAnimation(playerAnimation);
    exitManager.setPlayer(player);

    animationEventManager.addTimeline(backgroundManager.getBackground().getScrollTimeline());
    animationEventManager.addKeyPressedEvent(exitManager.getEventHandler());
    animationEventManager.addTimeline(player.getMoveTimeline());
    animationEventManager.addAnimation(player.getAnimation());
    animationEventManager.addKeyPressedEvent(player.getEventPressedHandler());
    animationEventManager.addKeyReleasedEvent(player.getEventReleasedHandler());

  }


  private void createAttackManager() {

    attackManager = new AttackManager(backgroundManager.getBackground().getContentPane(), player);
    attackManager.setCollisionManager(collisionManager);
    collisionManager.setSlingShotPane(attackManager.getWhipPane());

  }


  private void createEnemies() {

    enemyManager = new EnemyManager(level, backgroundManager.getBackground().getContentPane(), obstacleList, collisionManager, attackManager);
    for (Enemy e : enemyManager.getEnemiesList()) {
      e.setEffectManager(effectManager);

      animationEventManager.addTimeline(e.getMovementTimeline());
      animationEventManager.addAnimation(e.getAnimation());
    }

    collisionManager.setEnemies(enemyManager.getEnemiesList());
  }


  private void createInventory() {
    inventory = new Inventory(scene);
    inventory.setScrollPane(backgroundManager.getBackground().getScrollPane());
    inventory.setPlayer(player);
    player.setInventory(inventory);

    backgroundManager.getBackground().getContentPane().getChildren().add(inventory.getInventory());

    animationEventManager.addKeyPressedEvent(inventory.getEventHandler());
  }


  private void createGameMenu() {
    gameMenu = new GameMenu(scene);
    gameMenu.setScrollPane(backgroundManager.getBackground().getScrollPane());
    gameMenu.setInventory(inventory);
    player.setGameMenu(gameMenu);
    inventory.setGameMenu(gameMenu);
    attackManager.setGameMenu(gameMenu);

    backgroundManager.getBackground().getContentPane().getChildren().add(gameMenu.getGameMenu());

    gameMenu.setAnimationEventManager(animationEventManager);
    animationEventManager.addKeyPressedEvent(gameMenu.getEventHandler());
  }




  public void setScene(Scene scene) {
    this.scene = scene;
  }

  public void setLevel(int level) {
    this.level = level;
    System.out.println("LEVEL: " + level);
    initialize();
  }

}
