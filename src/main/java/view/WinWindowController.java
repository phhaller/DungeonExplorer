package view;

import database.HighscoreDatabaseManager;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.effect.Glow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.util.ArrayList;

public class WinWindowController {

  @FXML
  private AnchorPane background;

  private Scene scene;
  private int level;
  private Label backLabel;

  private Label yourScoreLabel;
  private Label highscoreLabel;
  private FadeTransition ft;

  private Label heartsLabel;
  private Label coinsLabel;
  private Label slimeLabel;
  private Label potionGreenLabel;
  private Label potionBlueLabel;
  private Label potionPurpleLabel;
  private Label potionRedLabel;
  private Label potionYellowLabel;

  private HighscoreDatabaseManager highscoreDatabaseManager;


  private void initialize() {

    highscoreDatabaseManager = new HighscoreDatabaseManager();

    createBackLabel();
    createWinWindow();
    createHandler();

  }


  private void createBackLabel() {
    backLabel = new Label();
    backLabel.setTranslateX(30);
    backLabel.setTranslateY(30);
    backLabel.setGraphic(new ImageView(new Image("/pictures/menu/backArrow.png")));
    backLabel.getStyleClass().add("backLabel");
    background.getChildren().add(backLabel);
  }


  private void createWinWindow() {

    int nodesStartY = 100;

    Label winLabel = new Label("LEVEL " + level + " COMPLETED!");
    winLabel.setPrefSize(800, 100);
    winLabel.setTranslateX(240);
    winLabel.setTranslateY(nodesStartY);
    winLabel.getStyleClass().add("winLabel");


    yourScoreLabel = new Label("SCORE: ");
    yourScoreLabel.setPrefSize(800, 100);
    yourScoreLabel.setTranslateX(240);
    yourScoreLabel.setTranslateY(nodesStartY + 160);
    yourScoreLabel.getStyleClass().add("yourScoreLabel");


    highscoreLabel = new Label("HIGHSCORE!");
    highscoreLabel.setPrefSize(500, 100);
    highscoreLabel.setTranslateX(850);
    highscoreLabel.setTranslateY(nodesStartY + 170);
    highscoreLabel.getStyleClass().add("highscoreLabel");
    highscoreLabel.setVisible(false);

    Glow glow = new Glow(.4);
    highscoreLabel.setEffect(glow);

    ft = new FadeTransition(Duration.millis(1300), highscoreLabel);
    ft.setFromValue(0.9);
    ft.setToValue(0.1);
    ft.setCycleCount(Animation.INDEFINITE);
    ft.setAutoReverse(true);


    Group healthGroup = new Group();
    healthGroup.setTranslateX(340);
    healthGroup.setTranslateY(nodesStartY + 370);

    Rectangle hearts = new Rectangle(32, 32);
    hearts.getStyleClass().add("healthRec");

    heartsLabel = new Label("3");
    heartsLabel.setPrefSize(70, 32);
    heartsLabel.setTranslateX(150);
    heartsLabel.getStyleClass().add("currentLabel");

    healthGroup.getChildren().addAll(hearts, createXLabel(110), heartsLabel);


    Group coinGroup = new Group();
    coinGroup.setTranslateX(340);
    coinGroup.setTranslateY(nodesStartY + 420);

    Rectangle coinsRec = new Rectangle(32, 32);
    coinsRec.getStyleClass().add("coin");

    coinsLabel = new Label("20");
    coinsLabel.setPrefSize(70, 32);
    coinsLabel.setTranslateX(150);
    coinsLabel.getStyleClass().add("currentLabel");

    coinGroup.getChildren().addAll(coinsRec, createXLabel(110), coinsLabel);


    Group slimeGroup = new Group();
    slimeGroup.setTranslateX(340);
    slimeGroup.setTranslateY(nodesStartY + 470);

    Rectangle slimeRec = new Rectangle(32, 32);
    slimeRec.getStyleClass().add("slime");

    slimeLabel = new Label("0");
    slimeLabel.setPrefSize(70, 32);
    slimeLabel.setTranslateX(150);
    slimeLabel.getStyleClass().add("currentLabel");

    slimeGroup.getChildren().addAll(slimeRec, createXLabel(110), slimeLabel);



    // RIGHT SIDE OF INVENTORY
    Group potionGreenGroup = new Group();
    potionGreenGroup.setTranslateX(720);
    potionGreenGroup.setTranslateY(nodesStartY + 320);

    Rectangle potionG = new Rectangle(32, 32);
    potionG.getStyleClass().add("potionGreen");

    potionGreenLabel = new Label("4");
    potionGreenLabel.setPrefSize(70, 32);
    potionGreenLabel.setTranslateX(150);
    potionGreenLabel.getStyleClass().add("currentLabel");

    potionGreenGroup.getChildren().addAll(potionG, createXLabel(110), potionGreenLabel);


    Group potionBlueGroup = new Group();
    potionBlueGroup.setTranslateX(720);
    potionBlueGroup.setTranslateY(nodesStartY + 370);

    Rectangle potionB = new Rectangle(32, 32);
    potionB.getStyleClass().add("potionBlue");

    potionBlueLabel = new Label("6");
    potionBlueLabel.setPrefSize(70, 32);
    potionBlueLabel.setTranslateX(150);
    potionBlueLabel.getStyleClass().add("currentLabel");

    potionBlueGroup.getChildren().addAll(potionB, createXLabel(110), potionBlueLabel);


    Group potionPurpleGroup = new Group();
    potionPurpleGroup.setTranslateX(720);
    potionPurpleGroup.setTranslateY(nodesStartY + 420);

    Rectangle potionP = new Rectangle(32, 32);
    potionP.getStyleClass().add("potionPurple");

    potionPurpleLabel = new Label("2");
    potionPurpleLabel.setPrefSize(70, 32);
    potionPurpleLabel.setTranslateX(150);
    potionPurpleLabel.getStyleClass().add("currentLabel");

    potionPurpleGroup.getChildren().addAll(potionP, createXLabel(110), potionPurpleLabel);


    Group potionRedGroup = new Group();
    potionRedGroup.setTranslateX(720);
    potionRedGroup.setTranslateY(nodesStartY + 470);

    Rectangle potionR = new Rectangle(32, 32);
    potionR.getStyleClass().add("potionRed");

    potionRedLabel = new Label("5");
    potionRedLabel.setPrefSize(70, 32);
    potionRedLabel.setTranslateX(150);
    potionRedLabel.getStyleClass().add("currentLabel");

    potionRedGroup.getChildren().addAll(potionR, createXLabel(110), potionRedLabel);


    Group potionYellowGroup = new Group();
    potionYellowGroup.setTranslateX(720);
    potionYellowGroup.setTranslateY(nodesStartY + 520);

    Rectangle potionY = new Rectangle(32, 32);
    potionY.getStyleClass().add("potionYellow");

    potionYellowLabel = new Label("8");
    potionYellowLabel.setPrefSize(70, 32);
    potionYellowLabel.setTranslateX(150);
    potionYellowLabel.getStyleClass().add("currentLabel");

    potionYellowGroup.getChildren().addAll(potionY, createXLabel(110), potionYellowLabel);


    background.getChildren().addAll(winLabel,yourScoreLabel, highscoreLabel, healthGroup, coinGroup, slimeGroup, potionGreenGroup,
        potionBlueGroup, potionPurpleGroup, potionRedGroup, potionYellowGroup);

  }

  private Label createXLabel(int x) {

    Label xLabel = new Label("x");
    xLabel.setPrefSize(32, 32);
    xLabel.setTranslateX(x);
    xLabel.getStyleClass().add("xLabel");

    return xLabel;

  }


  private void createHandler() {
    backLabel.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
      @Override
      public void handle(MouseEvent event) {
        switchScene();
      }
    });
  }


  private void switchScene() {

    try {

      highscoreDatabaseManager.closeConnection();

      if (ft != null) {
        ft.stop();
      }

      FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/startWindow.fxml"));
      Parent root = loader.load();
      Stage stage = (Stage) background.getScene().getWindow();

      StartWindowController controller = loader.getController();
      controller.setScene(scene);

      stage.getScene().setRoot(root);

    } catch (IOException e) {
      e.printStackTrace();
    }

  }





  public void setScene(Scene scene) {
    this.scene = scene;
  }

  public void setLevel(int level) {
    this.level = level;
    initialize();
  }

  public void setStats(int lives, int coins, int keys, int slimes, int greenPotion, int bluePotion, int purplePotion, int redPotion, int yellowPotion) {

    ArrayList<String> levelEntries = highscoreDatabaseManager.getLevelEntry(level);

    heartsLabel.setText(String.valueOf(lives));
    coinsLabel.setText(String.valueOf(coins));
    slimeLabel.setText(String.valueOf(slimes));
    potionGreenLabel.setText(String.valueOf(greenPotion));
    potionBlueLabel.setText(String.valueOf(bluePotion));
    potionPurpleLabel.setText(String.valueOf(purplePotion));
    potionRedLabel.setText(String.valueOf(redPotion));
    potionYellowLabel.setText(String.valueOf(yellowPotion));

    if (levelEntries.isEmpty()) {
      int highscore = computeScore(lives, coins, keys, slimes, greenPotion, bluePotion, purplePotion, redPotion,
          yellowPotion);
      yourScoreLabel.setText("SCORE:   " + highscore);
      highscoreLabel.setVisible(true);
      ft.play();
      highscoreDatabaseManager.createEntry(level, lives, coins, keys, slimes, greenPotion, bluePotion, purplePotion,
          redPotion, yellowPotion, highscore);
    } else {
      int highscore = highscoreDatabaseManager.getLevelHighscore(level);
      int current = computeScore(lives, coins, keys, slimes, greenPotion, bluePotion, purplePotion, redPotion,
          yellowPotion);
      yourScoreLabel.setText("SCORE:   " + current);
      if (current > highscore) {
        System.out.println("NEW HIGHSCORE!! " + highscore + " -> " + current);
        highscoreLabel.setVisible(true);
        ft.play();
        highscoreDatabaseManager.updateEntry(level, lives, coins, keys, slimes, greenPotion, bluePotion, purplePotion,
            redPotion, yellowPotion, current);
      }
    }



  }


  private int computeScore(int lives, int coins, int keys, int slimes, int greenPotion, int bluePotion,
                            int purplePotion, int redPotion, int yellowPotion) {

    int score = 0;

    score += lives * 10;
    score += coins * 2;
    score += keys * 5;
    score += slimes * 5;
    score += greenPotion * 2;
    score += bluePotion * 2;
    score += purplePotion * 2;
    score += redPotion * 2;
    score += yellowPotion * 2;

    return score;

  }


}
