package collision;

import background.Chunk;
import collectible.Collectible;
import enemy.Enemy;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import obstacle.Obstacle;
import player.Player;
import player.PlayerAnimation;
import utils.AnimationEventManager;

import java.util.ArrayList;

public class CollisionManager {

  private Line ground;
  private Player player;
  private PlayerAnimation playerAnimation;
  private ArrayList<Chunk> chunks;
  private Chunk activeChunk = null;
  private Obstacle currentObstacle = null;
  private ArrayList<Enemy> enemies;
  private Enemy currentEnemy = null;
  private Pane slingShotPane;

  private AnimationEventManager animationEventManager;


  public boolean checkWindowBorder() {

    boolean atBorder = false;

    if (playerAnimation.getTranslateX() <= 0) {
      playerAnimation.setTranslateX(playerAnimation.getTranslateX()+1);
      atBorder = true;
    }

    if (playerAnimation.getTranslateX() >= 9600f - playerAnimation.getWidth()) {
      playerAnimation.setTranslateX(playerAnimation.getTranslateX()-1);
      atBorder = true;
    }

    return atBorder;

  }


  public boolean checkGround() {

    boolean onGround = false;

    if (playerAnimation.getBoundsInParent().intersects(ground.getBoundsInParent())) {
      playerAnimation.setTranslateY(ground.getEndY() - playerAnimation.getSpriteHeight());
      onGround = true;
    }

    return onGround;

  }


  public String checkObstacle(float jumpStrength) {

    String collisionSide = "";

    for (Obstacle o : activeChunk.getObstaclesInChunk()) {

      if (playerAnimation.getBoundsInParent().intersects(o.getObstacle().getBoundsInParent())) {

        double bottomDiff = o.getObstacle().getBoundsInParent().getMaxY() - playerAnimation.getBoundsInParent().getMinY();
        double topDiff = playerAnimation.getBoundsInParent().getMaxY() - o.getObstacle().getBoundsInParent().getMinY();

        if (playerAnimation.getBoundsInParent().getMinY() < o.getObstacle().getBoundsInParent().getMaxY() &&
            bottomDiff < 20 && jumpStrength > 0) {
          playerAnimation.setTranslateY(playerAnimation.getTranslateY() + bottomDiff);
          collisionSide = "BOTTOM";
        }

        if (playerAnimation.getBoundsInParent().getMaxY() > o.getObstacle().getBoundsInParent().getMinY() &&
            topDiff < 20 && jumpStrength < 0) {
          playerAnimation.setTranslateY(playerAnimation.getTranslateY() - topDiff);
          currentObstacle = o;
          if (o.getType().equals("trap")) {
            if (player.getShield().isActive()) {
              player.getShield().deactivate();
            } else {
              player.getHealth().removeLife();
              if (player.getHealth().isDead()) {
                animationEventManager.stopGame();
              }
            }
          }
          collisionSide = "TOP";
        }

      }

    }

    return collisionSide;

  }


  public boolean checkSideCollision(double moveSpeed, String dir, boolean ON_OBSTACLE) {

    boolean collided = false;

    for (Obstacle o : activeChunk.getObstaclesInChunk()) {
      Rectangle oRec = o.getObstacle();

      if (playerAnimation.getBoundsInParent().intersects(oRec.getBoundsInParent())) {

        if (!ON_OBSTACLE || o.getType().equals("trap")) {

          double rightDiff = playerAnimation.getBoundsInParent().getMaxX() - oRec.getBoundsInParent().getMinX();
          double leftDiff = oRec.getBoundsInParent().getMaxX() - playerAnimation.getBoundsInParent().getMinX();

          if (dir.equals("RIGHT")) {
            if (playerAnimation.getBoundsInParent().getMaxX() > oRec.getBoundsInParent().getMinX() &&
                rightDiff < 10) {
              collided = true;
              currentObstacle = o;
              playerAnimation.setTranslateX(playerAnimation.getTranslateX() - moveSpeed);
            }
          }

          if (dir.equals("LEFT")) {
            if (playerAnimation.getBoundsInParent().getMinX() < oRec.getBoundsInParent().getMaxX() &&
                leftDiff < 10) {
              collided = true;
              currentObstacle = o;
              playerAnimation.setTranslateX(playerAnimation.getTranslateX() + moveSpeed);
            }
          }

        }
      }

    }

    return collided;

  }


  public String getObstacleType() {
    return currentObstacle.getType();
  }


  public boolean isWalkingOff() {

    boolean walkingOff = false;

    if (playerAnimation.getBoundsInParent().getMinX() > currentObstacle.getObstacle().getBoundsInParent().getMaxX() ||
        playerAnimation.getBoundsInParent().getMaxX() < currentObstacle.getObstacle().getBoundsInParent().getMinX()) {
      walkingOff = true;
    }

    return walkingOff;

  }


  public void checkCurrentChunk() {

    for (Chunk c : chunks) {
      if (playerAnimation.getBoundsInParent().intersects(c.getChunk().getBoundsInParent())) {
        if (c != activeChunk) {
          activeChunk = c;
        }
      }
    }

  }


  public void checkCollectibleCollision() {

    Chunk curChunk = activeChunk;

    for (Collectible c : curChunk.getCollectiblesInChunk()) {
      if (playerAnimation.getBoundsInParent().intersects(c.getCollectible().getBoundsInParent()) && !c.isCollected()) {
        c.collect(curChunk);

        if (c.getType().equals("coin")) {
          player.getInventory().setCoins(player.getInventory().getCoins() + 1);
        }

        if (c.getType().equals("key")) {
          player.getInventory().setKeys(player.getInventory().getKeys() + 1);
        }

        if (c.getType().equals("PotionGreen")) {
          player.getInventory().setPotionGreen(player.getInventory().getPotionGreen() + 1);
        }

        if (c.getType().equals("PotionBlue")) {
          player.getInventory().setPotionBlue(player.getInventory().getPotionBlue() + 1);
        }

        if (c.getType().equals("PotionPurple")) {
          player.getInventory().setPotionPurple(player.getInventory().getPotionPurple() + 1);
        }

        if (c.getType().equals("PotionRed")) {
          player.getInventory().setPotionRed(player.getInventory().getPotionRed() + 1);
        }

        if (c.getType().equals("PotionYellow")) {
          player.getInventory().setPotionYellow(player.getInventory().getPotionYellow() + 1);
        }
      }
    }

  }


  public ArrayList<Enemy> checkSlingShotEnemyCollision() {

    ArrayList<Enemy> enemyList = new ArrayList<>();

    for (Enemy e : enemies) {
      if (slingShotPane.isVisible() && slingShotPane.getBoundsInParent().intersects(e.getEnemy().getBoundsInParent())) {

        enemyList.add(e);
        System.out.println("THE SLING SHOT COLLIDED WITH AN ENEMY!!");

      }
    }

    if (!enemyList.isEmpty()) {
      for (Enemy enem : enemyList) {
        enemies.remove(enem);
      }
    }

    return enemyList;

  }


  public void checkPlayerEnemyCollision() {

    /* THIS IS NOT WORKING CORRECTLY. CURRENT ENEMY IS SET TO NULL EVEN THOUGH IT SHOULD NOT */

    /*if (currentEnemy != null) {
      if (!playerAnimation.getBoundsInParent().intersects(currentEnemy.getEnemy().getBoundsInParent())) {
        currentEnemy = null;
        System.out.println("Current Enemy: NULL");
      }
    }*/

    for (Enemy e : enemies) {
      if (playerAnimation.getBoundsInParent().intersects(e.getEnemy().getBoundsInParent())) {

        if (currentEnemy != e && e.isAlive()) {
          currentEnemy = e;

          if (player.getShield().isActive()) {
            player.getShield().deactivate();
          } else {
            player.getHealth().removeLife();
            if (player.getHealth().isDead()) {
              animationEventManager.stopGame();
            }
          }

          System.out.println("YOU HAVE COLLIDED WITH AN ENEMY!!");
        }

      }

    }

  }


  public String checkPlayerInSight(double x, double y, int distance) {

    String dir = "";

    if (playerAnimation.getTranslateX() >= x - distance &&
        playerAnimation.getTranslateX() <  x + 10 &&
        playerAnimation.getTranslateY() >= y - distance &&
        playerAnimation.getTranslateY() <= y + distance) {
      dir = "LEFT";
    }

    if (playerAnimation.getTranslateX() <= x + distance &&
        playerAnimation.getTranslateX() >  x + 22 &&
        playerAnimation.getTranslateY() >= y - distance &&
        playerAnimation.getTranslateY() <= y + distance) {
      dir = "RIGHT";
    }

    return dir;

  }


  public boolean checkShotObstacleIntersection(Circle shot) {

    boolean collided = false;

    for (Chunk c : chunks) {
      for (Obstacle o : c.getObstaclesInChunk()) {
        if (shot.getBoundsInParent().intersects(o.getObstacle().getBoundsInParent())) {
          collided = true;
        }
      }
    }

    return collided;

  }


  public boolean checkShotPlayerIntersection(Circle shot) {

    boolean collided = false;

    if (shot.getBoundsInParent().intersects(playerAnimation.getBoundsInParent())) {
      collided = true;
      player.getHealth().removeLife();
      if (player.getHealth().isDead()) {
        animationEventManager.stopGame();
      }
      System.out.println("PLAYER HIT");
    }

    return collided;

  }





  public void setGround(Line ground) {
    this.ground = ground;
  }

  public void setChunks(ArrayList<Chunk> chunks) {
    this.chunks = chunks;
  }

  public void setPlayer(Player player) {
    this.player = player;
  }

  public void setPlayerAnimation(PlayerAnimation playerAnimation) {
    this.playerAnimation = playerAnimation;
  }

  public void setEnemies(ArrayList<Enemy> enemies) {
    this.enemies = enemies;
  }

  public void setSlingShotPane(Pane slingShotPane) {
    this.slingShotPane = slingShotPane;
  }

  public Obstacle getCurrentObstacle() {
    return currentObstacle;
  }

  public double[] getPlayerPos() {
    return new double[] {playerAnimation.getTranslateX(), playerAnimation.getTranslateY()};
  }

  public void setAnimationEventManager(AnimationEventManager animationEventManager) {
    this.animationEventManager = animationEventManager;
  }
}
