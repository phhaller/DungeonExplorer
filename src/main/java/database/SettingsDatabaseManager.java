package database;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;

public class SettingsDatabaseManager {

  private String os = System.getProperty("os.name");
  private String userName = System.getProperty("user.name");

  private final String DB_NAME = "DungeonExplorer_DB.db";
  private String CONNECTION_STRING = "";

  private final String TABLE_SOUND = "sound";
  private final String COLUMN_ID = "id";
  private final String COLUMN_STOMP = "stomp";
  private final String COLUMN_WHIP = "whip";
  private final String COLUMN_SLIME = "slime";
  private final String COLUMN_GATE = "gate";
  private final String COLUMN_MUSIC = "music";

  private Connection connection;
  private Statement statement;


  public SettingsDatabaseManager() {

    if (os.startsWith("Mac")) {
      String dbPath = "/Users/" + userName + "/Documents/DungeonExplorer";
      boolean f = new File(dbPath).mkdirs();
      CONNECTION_STRING = "jdbc:sqlite:" + dbPath + "/" + DB_NAME;
    }

    if (os.startsWith("Windows")) {
      String dbPath = "C:\\Users\\" + userName + "\\Documents\\DungeonExplorer";
      boolean f = new File(dbPath).mkdirs();
      CONNECTION_STRING = "jdbc:sqlite:" + dbPath + "\\" + DB_NAME;
    }

    try {

      connection = DriverManager.getConnection(CONNECTION_STRING);
      statement = connection.createStatement();

      statement.execute("CREATE TABLE IF NOT EXISTS " +
          TABLE_SOUND + "( " +
          COLUMN_ID + " REAL, " +
          COLUMN_STOMP + " REAL, " +
          COLUMN_WHIP + " REAL, " +
          COLUMN_SLIME + " REAL, " +
          COLUMN_GATE + " REAL, " +
          COLUMN_MUSIC + " REAL)");

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }



  public void createEntry(double id, double stomp, double whip, double slime, double gate, double music) {

    try {

      statement.execute("INSERT INTO " +
          TABLE_SOUND + " VALUES " + "('" +
          id + "', '" +
          stomp + "', '" +
          whip + "', '" +
          slime + "', '" +
          gate + "', '" +
          music + "')");

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }



  public void updateEntry(double id, double stomp, double whip, double slime, double gate, double music) {

    try {

      statement.execute("UPDATE " + TABLE_SOUND +
          " SET " + COLUMN_ID + " = '" + id + "'" + ", " +
          COLUMN_STOMP + " = '" + stomp + "'" + ", " +
          COLUMN_WHIP + " = '" + whip + "'" + ", " +
          COLUMN_SLIME + " = '" + slime + "'" + ", " +
          COLUMN_GATE + " = '" + gate + "'" + ", " +
          COLUMN_MUSIC + " = '" + music + "'" +
          " WHERE " + COLUMN_ID + " = '" + id + "'" + "COLLATE  NOCASE");

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }



  public ArrayList<String> getIdEntry(double id) {

    ResultSet resultSet = null;

    try {
      resultSet = statement.executeQuery("SELECT * FROM " + TABLE_SOUND +
          " WHERE " + COLUMN_ID + " = '" + id + "'");
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return getResults(resultSet);

  }











  private ArrayList<String> getResults(ResultSet resultSet) {

    ArrayList<String> entries = new ArrayList<>();

    try {

      while (resultSet.next()) {
        entries.add(resultSet.getInt(COLUMN_ID) + "§" +
            resultSet.getDouble(COLUMN_STOMP) + "§" +
            resultSet.getDouble(COLUMN_WHIP) + "§" +
            resultSet.getDouble(COLUMN_SLIME) + "§" +
            resultSet.getDouble(COLUMN_GATE) + "§" +
            resultSet.getDouble(COLUMN_MUSIC)
        );
      }

    } catch (SQLException e ) {
      e.printStackTrace();
    }

    return entries;

  }



  public void closeConnection() {
    try {
      connection.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }



}
