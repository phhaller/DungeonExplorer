package database;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;

public class HighscoreDatabaseManager {

  private String os = System.getProperty("os.name");
  private String userName = System.getProperty("user.name");

  private final String DB_NAME = "DungeonExplorer_DB.db";
  private String CONNECTION_STRING = "";

  private final String TABLE_HIGHSCORES = "highscores";
  private final String COLUMN_LEVEL = "level";
  private final String COLUMN_LIVES = "lives";
  private final String COLUMN_COINS = "coins";
  private final String COLUMN_KEYS = "keys";
  private final String COLUMN_SLIMES = "slimes";
  private final String COLUMN_POTION_GREEN = "potion_green";
  private final String COLUMN_POTION_BLUE = "potion_blue";
  private final String COLUMN_POTION_PURPLE = "potion_purple";
  private final String COLUMN_POTION_RED = "potion_red";
  private final String COLUMN_POTION_YELLOW = "potion_yellow";
  private final String COLUMM_HIGHSCORE = "highscore";

  private Connection connection;
  private Statement statement;


  public HighscoreDatabaseManager() {

    if (os.startsWith("Mac")) {
      String dbPath = "/Users/" + userName + "/Documents/DungeonExplorer";
      boolean f = new File(dbPath).mkdirs();
      CONNECTION_STRING = "jdbc:sqlite:" + dbPath + "/" + DB_NAME;
    }

    if (os.startsWith("Windows")) {
      String dbPath = "C:\\Users\\" + userName + "\\Documents\\DungeonExplorer";
      boolean f = new File(dbPath).mkdirs();
      CONNECTION_STRING = "jdbc:sqlite:" + dbPath + "\\" + DB_NAME;
    }


    try {

      connection = DriverManager.getConnection(CONNECTION_STRING);
      statement = connection.createStatement();

      statement.execute("CREATE TABLE IF NOT EXISTS " +
          TABLE_HIGHSCORES + "( " +
          COLUMN_LEVEL + " INTEGER, " +
          COLUMN_LIVES + " INTEGER, " +
          COLUMN_COINS + " INTEGER, " +
          COLUMN_KEYS + " INTEGER, " +
          COLUMN_SLIMES + " INTEGER, " +
          COLUMN_POTION_GREEN + " INTEGER, " +
          COLUMN_POTION_BLUE + " INTEGER, " +
          COLUMN_POTION_PURPLE + " INTEGER, " +
          COLUMN_POTION_RED + " INTEGER, " +
          COLUMN_POTION_YELLOW + " INTEGER, " +
          COLUMM_HIGHSCORE + " INTEGER)");

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }



  public void createEntry(int level, int lives, int coins, int keys, int slimes, int potion_green, int potion_blue,
                          int potion_purple, int potion_red, int potion_yellow, int highscore) {

    try {

      statement.execute("INSERT INTO " +
          TABLE_HIGHSCORES + " VALUES " + "('" +
          level + "', '" +
          lives + "', '" +
          coins + "', '" +
          keys + "', '" +
          slimes + "', '" +
          potion_green + "', '" +
          potion_blue + "', '" +
          potion_purple + "', '" +
          potion_red + "', '" +
          potion_yellow + "', '" +
          highscore + "')");

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }



  public void updateEntry(int level, int lives, int coins, int keys, int slimes, int potion_green, int potion_blue,
                          int potion_purple, int potion_red, int potion_yellow, int highscore) {

    try {

      statement.execute("UPDATE " + TABLE_HIGHSCORES +
          " SET " + COLUMN_LIVES + " = '" + lives + "'" + ", " +
          COLUMN_COINS + " = '" + coins + "'" + ", " +
          COLUMN_KEYS + " = '" + keys + "'" + ", " +
          COLUMN_SLIMES + " = '" + slimes + "'" + ", " +
          COLUMN_POTION_GREEN + " = '" + potion_green + "'" + ", " +
          COLUMN_POTION_BLUE + " = '" + potion_blue + "'" + ", " +
          COLUMN_POTION_PURPLE + " = '" + potion_purple + "'" + ", " +
          COLUMN_POTION_RED + " = '" + potion_red + "'" + ", " +
          COLUMN_POTION_YELLOW + " = '" + potion_yellow + "'" + ", " +
          COLUMM_HIGHSCORE + " = '" + highscore + "'" +
          " WHERE " + COLUMN_LEVEL + " = '" + level + "'" + "COLLATE  NOCASE");

    } catch (SQLException e) {
      e.printStackTrace();
    }

  }



  public ArrayList<String> getLevelEntry(int level) {

    ResultSet resultSet = null;

    try {
      resultSet = statement.executeQuery("SELECT * FROM " + TABLE_HIGHSCORES +
          " WHERE " + COLUMN_LEVEL + " = '" + level + "'");
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return getResults(resultSet);

  }



  public int getLevelHighscore(int level) {

    int high = 0;

    try {
      ResultSet resultSet = statement.executeQuery("SELECT " + COLUMM_HIGHSCORE + " AS highscore FROM " + TABLE_HIGHSCORES +
          " WHERE " + COLUMN_LEVEL + " = '" + level + "'");
      if (resultSet.next()) {
        high = resultSet.getInt("highscore");
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return high;

  }



  public int getMaxLevel() {

    int max = 0;

    try {
      ResultSet resultSet = statement.executeQuery("SELECT  COUNT(*) AS total FROM " + TABLE_HIGHSCORES);
      max = resultSet.getInt("total");
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return max;

  }



  private ArrayList<String> getResults(ResultSet resultSet) {

    ArrayList<String> entries = new ArrayList<>();

    try {

      while (resultSet.next()) {
        entries.add(resultSet.getInt(COLUMN_LEVEL) + "§" +
            resultSet.getInt(COLUMN_LIVES) + "§" +
            resultSet.getInt(COLUMN_COINS) + "§" +
            resultSet.getInt(COLUMN_KEYS) + "§" +
            resultSet.getInt(COLUMN_SLIMES) + "§" +
            resultSet.getInt(COLUMN_POTION_GREEN) + "§" +
            resultSet.getInt(COLUMN_POTION_BLUE) + "§" +
            resultSet.getInt(COLUMN_POTION_PURPLE) + "§" +
            resultSet.getInt(COLUMN_POTION_RED) + "§" +
            resultSet.getInt(COLUMN_POTION_YELLOW) + "§" +
            resultSet.getInt(COLUMM_HIGHSCORE)
        );
      }

    } catch (SQLException e ) {
      e.printStackTrace();
    }

    return entries;

  }



  public void closeConnection() {
    try {
      connection.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }




}
