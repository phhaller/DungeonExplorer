package utils;

import java.io.InputStream;
import java.util.Scanner;

public class CSVReader {

  String[][] csvMap;


  public CSVReader(String filename) {

    readCSVFile(filename);

  }


  private void readCSVFile(String filename) {

    int counter = 0;
    csvMap = new String[150][30];

    InputStream inputStream = getClass().getResourceAsStream("/csv/" + filename);
    Scanner s = new Scanner(inputStream);
    s.useDelimiter(",");
    while (s.hasNextLine()) {

      String col = s.nextLine();
      String[] entries = col.split(",");

      for (int i = 0; i < entries.length; i++) {
        csvMap[i][counter] = getTileType(Integer.parseInt(entries[i]));
      }

      counter++;

    }

  }


  private String getTileType(int tile) {

    String tileType = "";

    // NO TILE
    if (tile == -1) {
      tileType = "NULL";
    }



    // GROUND TILES
    if (tile == 40 || tile == 41 || tile == 42 || tile == 43 || tile == 44 || tile == 45 || tile == 46 || tile == 47) {
      tileType = "groundSmall";
    }


    if (tile == 0  || tile == 1 || tile == 2 || tile == 3 || tile == 4 || tile == 5 || tile == 6 || tile == 9 ||
        tile == 80 || tile == 81 || tile == 82 || tile == 83 || tile == 84 || tile == 85 || tile == 86 || tile == 87) {
      tileType = "groundLarge";
    }



    // WALL TILES
    if (tile == 120) {
      tileType = "wallRegular";
    }


    if (tile == 7 || tile == 8 || tile == 10 || tile == 11 || tile == 160 || tile == 161 || tile == 162 || tile == 163) {
      tileType = "wallSticky";
    }



    // TRAPS
    if (tile == 280) {
      tileType = "trap";
    }



    // COLLECTIBLES
    if (tile == 320) {
      tileType = "collectibleCoin";
    }

    if (tile == 321) {
      tileType = "collectibleKey";
    }

    if (tile == 322) {
      tileType = "collectiblePotionGreen";
    }

    if (tile == 323) {
      tileType = "collectiblePotionRed";
    }

    if (tile == 324) {
      tileType = "collectiblePotionPurple";
    }

    if (tile == 325) {
      tileType = "collectiblePotionBlue";
    }

    if (tile == 326) {
      tileType = "collectiblePotionYellow";
    }



    // EXIT
    if (tile == 360) {
      tileType = "exit";
    }



    return tileType;

  }


  public String[][] getCsvMap() {
    return csvMap;
  }


}
