package utils;

import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.media.MediaPlayer;

import java.util.ArrayList;

public class AnimationEventManager {

  private Scene scene;

  private ArrayList<EventHandler<KeyEvent>> keyPressedEvents;
  private ArrayList<EventHandler<KeyEvent>> keyReleasedEvents;
  private ArrayList<Timeline> timelines;
  private ArrayList<SpriteAnimation> animations;
  private ArrayList<MediaPlayer> mediaPlayers;




  public AnimationEventManager(Scene scene) {

    this.scene = scene;

    keyPressedEvents = new ArrayList<>();
    keyReleasedEvents = new ArrayList<>();
    timelines = new ArrayList<>();
    animations = new ArrayList<>();
    mediaPlayers = new ArrayList<>();

  }



  public void addKeyPressedEvent(EventHandler<KeyEvent> eventHandler) {
    keyPressedEvents.add(eventHandler);
  }


  public void addKeyReleasedEvent(EventHandler<KeyEvent> eventHandler) {
    keyReleasedEvents.add(eventHandler);
  }


  public void addTimeline(Timeline timeline) {
    timelines.add(timeline);
  }


  public void addAnimation(SpriteAnimation animation) {
    animations.add(animation);
  }


  public void addMediaPlayers(ArrayList<MediaPlayer> mediaPlayers) {
    this.mediaPlayers = mediaPlayers;
  }


  public void stopGame() {

    for (EventHandler<KeyEvent> e : keyPressedEvents) {
      scene.removeEventHandler(KeyEvent.KEY_PRESSED, e);
    }

    for (EventHandler<KeyEvent> e : keyReleasedEvents) {
      scene.removeEventHandler(KeyEvent.KEY_RELEASED, e);
    }

    for (Timeline t : timelines) {
      t.stop();
    }

    for (SpriteAnimation s : animations) {
      s.stop();
    }

    for (MediaPlayer m : mediaPlayers) {
      m.stop();
    }


  }



}
